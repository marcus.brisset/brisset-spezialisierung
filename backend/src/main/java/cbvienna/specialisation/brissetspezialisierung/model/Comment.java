package cbvienna.specialisation.brissetspezialisierung.model;

import cbvienna.specialisation.brissetspezialisierung.dto.CommentDTO;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity(name = "comment")
public class Comment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(nullable = false,
			updatable = false,
			columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@CreationTimestamp
	private Timestamp timestamp;

	@Column(nullable = false,
			columnDefinition = "TEXT")
	private String text;

	@ManyToOne
	@JoinColumn(name = "event_id")
	private Event event;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	public Comment() {}

	public Comment(Event event, User user, String text) {
		this.event = event;
		this.user = user;
		this.text = text;
	}

	public CommentDTO toCommentDTO() {
		CommentDTO commentDTO = new CommentDTO();

		commentDTO.setId(this.id);
		commentDTO.setTimestamp(this.timestamp);
		commentDTO.setEvent(this.event);
		commentDTO.setUser(this.user);
		commentDTO.setText(this.text);

		return commentDTO;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
