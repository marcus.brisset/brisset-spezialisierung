package cbvienna.specialisation.brissetspezialisierung.model;

import cbvienna.specialisation.brissetspezialisierung.dto.UserDTO;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(columnDefinition = "VARCHAR(5) DEFAULT 'USER'")
	@Enumerated(EnumType.STRING)
	private UserType type = UserType.USER;

	@Column(nullable = false)
	private String name;

	@Column(nullable = false)
	private String password;

	@Column(nullable = false, unique = true)
	private String email;

	@OneToMany(mappedBy = "user")
	private Set<Request> requestSet;

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	private Set<Comment> commentSet;

	@ManyToMany(mappedBy = "attendsEvent")
	private Set<Event> attendedEvents = new HashSet<>();

	public  User() {}

	public User(String name, String password, String email) {
		this.name = name;
		this.password = password;
		this.email = email;

	}

	public User(UserType type, String name, String password, String email) {
		this.type = type;
		this.name = name;
		this.password = password;
		this.email = email;
	}

	public UserDTO toUserDTO() {
		UserDTO userDTO = new UserDTO();

		userDTO.setId(this.id);
		userDTO.setName(this.name);
		userDTO.setEmail(this.email);
		userDTO.setTotalAttendedEvents(this.attendedEvents.size());
		userDTO.setTotalCommentsOnEvents(this.commentSet.size());

		return userDTO;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public UserType getType() {
		return type;
	}

	public void setType(UserType type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<Request> getRequestSet() {
		return requestSet;
	}

	public void setRequestSet(Request request) {
		this.requestSet.add(request);
	}

	public Set<Event> getAttendedEvents() {
		return attendedEvents;
	}

	public void setAttendedEvents(Set<Event> attendedEvents) {
		this.attendedEvents = attendedEvents;
	}

	public void addAttendedEvent(Event event) {
		this.attendedEvents.add(event);
	}

	public void removeAttendedEvent(Event event) {
		this.attendedEvents.remove(event);
	}

	public Set<Comment> getCommentSet() {
		return commentSet;
	}

	public void setCommentSet(Comment comment) {
		this.commentSet.add(comment);
	}
}
