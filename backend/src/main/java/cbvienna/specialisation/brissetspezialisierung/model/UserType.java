package cbvienna.specialisation.brissetspezialisierung.model;

public enum UserType {

	ADMIN,
	USER;

}
