package cbvienna.specialisation.brissetspezialisierung.model;

import cbvienna.specialisation.brissetspezialisierung.dto.EventTypeDTO;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "event_type")
public class EventType {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(nullable = false)
	private String title;

	@OneToMany(mappedBy = "eventType", cascade = CascadeType.ALL)
	private Set<Event> eventSet = new HashSet<>();

	public EventType() {}

	public EventType(String title) {
		this.title = title;
	}

	public EventTypeDTO toEventTypeDTO() {
		EventTypeDTO eventTypeDTO = new EventTypeDTO();

		eventTypeDTO.setId(this.id);
		eventTypeDTO.setTitle(this.title);
		eventTypeDTO.setTotalEventsOfType(this.getEventSet().size());

		return eventTypeDTO;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Set<Event> getEventSet() {
		return eventSet;
	}

	public void setEventSet(Event event) {
		this.eventSet.add(event);
	}

}
