package cbvienna.specialisation.brissetspezialisierung.model;

import cbvienna.specialisation.brissetspezialisierung.dto.EventDTO;
import cbvienna.specialisation.brissetspezialisierung.dto.EventShortDTO;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Set;

@Entity(name="event")
public class Event {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(nullable = false,
			updatable = false,
			columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@CreationTimestamp
	private Timestamp timestamp;

	@Column(nullable = false)
	private String title;

	@Column(nullable = false, unique = true)
	private Date dateStart;

	@Column(nullable = false, unique = true)
	private Date dateEnd;

	@Column(nullable = false,
			columnDefinition = "DECIMAL(10,2)")
	private Double price;

	@Column(nullable = false)
	private Time doorsOpen;

	@Column(nullable = false)
	private String image;

	@Column(nullable = false,
			columnDefinition = "TEXT")
	private String text;

	@ManyToOne
	@JoinColumn(name = "event_type_id")
	private EventType eventType;

	@ManyToMany(cascade = CascadeType.PERSIST)
	@JoinTable(
			name = "attended_events",
			joinColumns = @JoinColumn(name = "event_id"),
			inverseJoinColumns = @JoinColumn(name = "user_id"))
	private Set<User> attendsEvent;

	@OneToMany(mappedBy = "event", cascade = CascadeType.ALL)
	private Set<Comment> commentSet;

	public Event() {}

	public Event(EventType eventType, String title, Date dateStart, Date dateEnd, Double price, Time doorsOpen, String image, String text) {
		this.eventType = eventType;
		this.title = title;
		this.dateStart = dateStart;
		this.dateEnd = dateEnd;
		this.price = price;
		this.doorsOpen = doorsOpen;
		this.image = image;
		this.text = text;
	}

	public EventShortDTO toShortDTO() {
		return new EventShortDTO(
				this.id, this.title, this.image
		);
	}

	public EventDTO toEventDTO() {
		EventDTO eventDTO = new EventDTO();

		eventDTO.setId(this.id);
		eventDTO.setTimestamp(this.timestamp);
		eventDTO.setTitle(this.title);
		eventDTO.setDateStart(this.dateStart);
		eventDTO.setDateEnd(this.dateEnd);
		eventDTO.setPrice(this.price);
		eventDTO.setDoorsOpen(this.doorsOpen);
		eventDTO.setImage(this.image);
		eventDTO.setText(this.text);
		eventDTO.setEventType(this.eventType);
		eventDTO.setTotalCommentsOnEvent(this.getCommentSet().size());
		eventDTO.setTotalUsersAttendingEvent(this.getAttendsEvent().size());
		eventDTO.setEventDurationInDays(eventDTO.calcEventDurationInDays());

		return eventDTO;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getDateStart() {
		return dateStart;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}

	public Date getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Time getDoorsOpen() {
		return doorsOpen;
	}

	public void setDoorsOpen(Time doors_open) {
		this.doorsOpen = doors_open;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Set<User> getAttendsEvent() {
		return attendsEvent;
	}

	public void setAttendsEvent(Set<User> attendsEvent) { this.attendsEvent = attendsEvent;	}

	public void addAttendsEvent(User user) {
		this.attendsEvent.add(user);
	}

	public void removeAttendsEvent(User user) {
		this.attendsEvent.remove(user);
	}

	public EventType getEventType() {
		return eventType;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}

	public Set<Comment> getCommentSet() {
		return commentSet;
	}

	public void setCommentSet(Comment comment) {
		this.commentSet.add(comment);
	}

}
