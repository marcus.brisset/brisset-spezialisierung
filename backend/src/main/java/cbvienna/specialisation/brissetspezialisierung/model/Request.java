package cbvienna.specialisation.brissetspezialisierung.model;

import cbvienna.specialisation.brissetspezialisierung.dto.RequestDTO;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

@Entity(name = "request")
public class Request {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(nullable = false,
			updatable = false,
			columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
	@CreationTimestamp
	private Timestamp timestamp;

	@Column(nullable = false)
	private Date eventDateStart;

	@Column(nullable = false)
	private Date eventDateEnd;

	@Column(nullable = false)
	private String name;

	@Column(nullable = false)
	private String email;

	@Column(nullable = false)
	private String phoneNr;

	@Column(nullable = false,
			columnDefinition = "TEXT")
	private String message;

	@Column(nullable = false,
			columnDefinition = "BOOLEAN DEFAULT TRUE")
	private boolean isNew;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	public Request() {}

	public Request(Date eventDateStart, Date eventDateEnd, String name, String email, String phoneNr, String message) {
		this.eventDateStart = eventDateStart;
		this.eventDateEnd = eventDateEnd;
		this.name = name;
		this.email = email;
		this.phoneNr = phoneNr;
		this.message = message;
		this.isNew = true;
	}

	public RequestDTO toRequestDTO() {
		RequestDTO requestDTO = new RequestDTO();

		requestDTO.setId(this.id);
		requestDTO.setTimestamp(this.timestamp);
		requestDTO.setEventDateStart(this.eventDateStart);
		requestDTO.setEventDateEnd(this.eventDateEnd);
		requestDTO.setName(this.name);
		requestDTO.setEmail(this.email);
		requestDTO.setPhoneNr(this.phoneNr);
		requestDTO.setMessage(this.message);
		requestDTO.setNew(this.isNew);

		return requestDTO;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public Date getEventDateStart() {
		return eventDateStart;
	}

	public void setEventDateStart(Date eventDateStart) {
		this.eventDateStart = eventDateStart;
	}

	public Date getEventDateEnd() {
		return eventDateEnd;
	}

	public void setEventDateEnd(Date eventDateEnd) {
		this.eventDateEnd = eventDateEnd;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNr() {
		return phoneNr;
	}

	public void setPhoneNr(String phoneNr) {
		this.phoneNr = phoneNr;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean aNew) {
		isNew = aNew;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
