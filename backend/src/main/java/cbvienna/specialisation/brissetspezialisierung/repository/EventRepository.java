package cbvienna.specialisation.brissetspezialisierung.repository;

import cbvienna.specialisation.brissetspezialisierung.model.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.sql.Date;
import java.util.List;

@Repository
@Transactional
public class EventRepository {

	@Autowired
	EntityManager em;

/*
	POST
*/

	public void addEvent(Event event) {
		em.persist(event);
		em.flush();
	}

/*
	PUT
*/

	public void updateEventData(Event event) {
		em.merge(event);
		em.flush();
	}

/*
	DELETE
*/

	public void deleteEvent(Event event) {
		em.remove(event);
		em.flush();
	}

/*
	GET
*/

	public List<Event> getEvents() {
		TypedQuery<Event> query = em.createQuery("SELECT e FROM event e ORDER BY e.timestamp DESC", Event.class);
		List<Event> eventList = query.getResultList();

		return !eventList.isEmpty()
				? eventList
				: null;
	}

	public List<Event> getEventsPaginated(Integer page) {
		int eventsPerPage = 10;
		int limitStart = (page - 1) * eventsPerPage;
		TypedQuery<Event> query = em.createQuery("SELECT e FROM event e ORDER BY e.timestamp DESC", Event.class);
		query.setFirstResult(limitStart);
		query.setMaxResults(eventsPerPage);

		return query.getResultList();
	}

	public Event getEventById(Integer id) {
		TypedQuery<Event> query = em.createQuery("SELECT e FROM event e WHERE e.id = ?1", Event.class);
		query.setParameter(1, id);

		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<Event> getEventsByTitle(String title) {
		TypedQuery<Event> query = em.createQuery("SELECT e FROM event e WHERE e.title LIKE ?1", Event.class);
		query.setParameter(1, "%" + title + "%");

		return query.getResultList();
	}

	public List<Event> getEventsBySearchTerm(String searchTerm) {
		TypedQuery<Event> query = em.createQuery("SELECT e FROM event e WHERE e.eventType.title LIKE ?1 OR e.title LIKE ?1 OR e.text LIKE ?1 ORDER BY e.dateStart", Event.class);
		query.setParameter(1, "%" + searchTerm + "%");

		return query.getResultList();
	}

	public List<Event> getEventsByMonth(Integer year, Integer month) {
		TypedQuery<Event> query = em.createQuery("SELECT e FROM event e WHERE YEAR(e.dateStart) = ?1 AND (MONTH(e.dateStart) = ?2 OR MONTH(e.dateEnd) = ?2) ORDER BY e.dateStart", Event.class);
		query.setParameter(1, year);
		query.setParameter(2, month);

		return query.getResultList();
	}

	public boolean dateNotAvailable(Date dateStart, Date dateEnd) {
		Query query = em.createQuery(
				"SELECT e.id FROM event e WHERE ((e.dateStart <= ?1 AND e.dateEnd >= ?1) OR (e.dateStart <= ?2 AND e.dateEnd >= ?2))" +
						" OR (e.dateStart >= ?1 AND e.dateEnd <= ?2)"
		);
		query.setParameter(1, dateStart);
		query.setParameter(2, dateEnd);

		return query.getResultList().size() > 0;
	}

	public boolean dateNotAvailable(Date dateStart, Date dateEnd, Integer eventId) {
		Query query = em.createQuery(
				"SELECT e.id FROM event e WHERE (((e.dateStart <= ?1 AND e.dateEnd >= ?1) OR (e.dateStart <= ?2 AND e.dateEnd >= ?2))" +
						" OR (e.dateStart >= ?1 AND e.dateEnd <= ?2)) AND e.id <> ?3"
		);
		query.setParameter(1, dateStart);
		query.setParameter(2, dateEnd);
		query.setParameter(3, eventId);

		return query.getResultList().size() > 0;
	}

}
