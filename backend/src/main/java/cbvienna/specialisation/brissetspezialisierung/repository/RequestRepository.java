package cbvienna.specialisation.brissetspezialisierung.repository;

import cbvienna.specialisation.brissetspezialisierung.model.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class RequestRepository {

	@Autowired
	EntityManager em;

/*
	POST
*/

	public void addRequest(Request request) {
		em.persist(request);
		em.flush();
	}

/*
	PUT
*/

	public void updateRequestData(Request request) {
		em.merge(request);
		em.flush();
	}

/*
	DELETE
*/

	public void deleteRequest(Request request) {
		em.remove(request);
		em.flush();
	}

/*
	GET
*/

	public List<Request> getRequests() {
		TypedQuery<Request> query = em.createQuery("SELECT r FROM request r ORDER BY r.timestamp DESC", Request.class);
		List<Request> requestList = query.getResultList();

		return !requestList.isEmpty()
				? requestList
				: null;
	}

	public List<Request> getRequestPaginated(Integer page) {
		int requestsPerPage = 10;
		int limitStart = (page - 1) * requestsPerPage;
		TypedQuery<Request> query = em.createQuery("SELECT r FROM request r ORDER BY r.timestamp DESC", Request.class);
		query.setFirstResult(limitStart);
		query.setMaxResults(requestsPerPage);

		return query.getResultList();
	}

	public Request getRequestById(Integer id) {
		TypedQuery<Request> query = em.createQuery("SELECT r FROM request r WHERE r.id = ?1", Request.class);
		query.setParameter(1, id);

		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<Request> getRequestsByName(String name) {
		TypedQuery<Request> query = em.createQuery("SELECT r FROM request r WHERE r.name LIKE ?1", Request.class);
		query.setParameter(1, "%" + name + "%");

		return query.getResultList();
	}

}
