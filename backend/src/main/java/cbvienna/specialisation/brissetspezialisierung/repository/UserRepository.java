package cbvienna.specialisation.brissetspezialisierung.repository;

import cbvienna.specialisation.brissetspezialisierung.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class UserRepository {

	@Autowired
	EntityManager em;

	@Autowired
	EventRepository eventRepository;

/*
	POST
*/

	public void addUser(User user) {
		em.persist(user);
		em.flush();
	}

/*
	PUT
*/

	public void updateUserData(User user) {
		em.merge(user);
		em.flush();
	}

/*
	DELETE
*/

	public void deleteUser(User user) {
		em.remove(user);
		em.flush();
	}

/*
	GET
*/

	public List<User> getUsers() {
		TypedQuery<User> query = em.createQuery("SELECT u FROM user u WHERE u.type <> 'ADMIN' ORDER BY u.id ASC", User.class);
		List<User> userList = query.getResultList();

		return userList.size() != 0
				? userList
				: null;
	}

	public List<User> getUsersPaginated(Integer page) {
		int usersPerPage = 10;
		int limitStart = (page - 1) * usersPerPage;
		TypedQuery<User> query = em.createQuery("SELECT u FROM user u WHERE u.type <> 'ADMIN' ORDER BY u.id ASC", User.class);
		query.setFirstResult(limitStart);
		query.setMaxResults(usersPerPage);

		return query.getResultList();
	}

	public List<User> getAllAccounts() {
		TypedQuery<User> query = em.createQuery("SELECT u FROM user u", User.class);
		List<User> userList = query.getResultList();

		return userList.size() != 0
				? userList
				: null;
	}

	public User getUserById(Integer id) {
		TypedQuery<User> query = em.createQuery("SELECT u FROM user u WHERE u.id = ?1", User.class);
		query.setParameter(1, id);

		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<User> getUsersByName(String name) {
		TypedQuery<User> query = em.createQuery("SELECT u FROM user u WHERE u.name LIKE ?1", User.class);
		query.setParameter(1, "%" + name + "%");

		return query.getResultList();
	}

	public User getUserByEmail(String email) {
		TypedQuery<User> query = em.createQuery("SELECT u FROM user u WHERE u.email = ?1", User.class);
		query.setParameter(1, email);

		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public boolean checkAttendance(Integer userId, Integer eventId) {
		TypedQuery<User> query = em.createQuery("SELECT u FROM user u WHERE u.id = ?1", User.class);
		query.setParameter(1, userId);

		try {
			User user = query.getSingleResult();
			return user.getAttendedEvents().stream().anyMatch(event -> event.getId() == eventId);
		} catch (NoResultException e) {
			return false;
		}
	}

}
