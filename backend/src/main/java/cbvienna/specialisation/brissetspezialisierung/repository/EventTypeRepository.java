package cbvienna.specialisation.brissetspezialisierung.repository;

import cbvienna.specialisation.brissetspezialisierung.model.EventType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class EventTypeRepository {

	@Autowired
	EntityManager em;

/*
	POST
*/

	public void addEventType(EventType eventType) {
		em.persist(eventType);
		em.flush();
	}

/*
	PUT
*/

	public void updateEventTypeData(EventType eventType) {
		em.merge(eventType);
		em.flush();
	}

/*
	DELETE
*/

	public void deleteEventType(EventType eventType) {
		em.remove(eventType);
		em.flush();
	}

/*
	GET
*/

	public List<EventType> getEventTypes() {
		TypedQuery<EventType> query = em.createQuery("SELECT et FROM event_type et", EventType.class);
		List<EventType> eventTypeList = query.getResultList();

		return !eventTypeList.isEmpty()
				? eventTypeList
				: null;
	}

	public EventType getEventTypeById(Integer id) {
		TypedQuery<EventType> query = em.createQuery("SELECT et FROM event_type et WHERE et.id = ?1", EventType.class);
		query.setParameter(1, id);

		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<EventType> getEventTypesByTitle(String title) {
		TypedQuery<EventType> query = em.createQuery("SELECT et FROM event_type et WHERE et.title LIKE ?1", EventType.class);
		query.setParameter(1, "%" + title + "%");

		return query.getResultList();
	}

}
