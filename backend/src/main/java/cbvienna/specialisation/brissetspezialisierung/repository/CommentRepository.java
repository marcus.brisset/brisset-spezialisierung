package cbvienna.specialisation.brissetspezialisierung.repository;

import cbvienna.specialisation.brissetspezialisierung.model.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class CommentRepository {

	@Autowired
	EntityManager em;

/*
	POST
*/

	public void addComment(Comment comment) {
		em.persist(comment);
		em.flush();
	}

/*
	PUT
*/

	public void updateCommentData(Comment comment) {
		em.merge(comment);
		em.flush();
	}

/*
	DELETE
*/

	public void deleteComment(Comment comment) {
		em.remove(comment);
		em.flush();
	}

/*
	GET
*/

	public List<Comment> getComments() {
		TypedQuery<Comment> query = em.createQuery("SELECT c FROM comment c", Comment.class);
		List<Comment> commentList = query.getResultList();

		return !commentList.isEmpty()
				? commentList
				: null;
	}

	public Comment getCommentById(Integer id) {
		TypedQuery<Comment> query = em.createQuery("SELECT c FROM comment c WHERE c.id = ?1", Comment.class);
		query.setParameter(1, id);

		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<Comment> getCommentsOnEvent(Integer id) {
		TypedQuery<Comment> query = em.createQuery("SELECT c FROM comment c WHERE c.event.id = ?1 ORDER BY c.timestamp DESC", Comment.class);
		query.setParameter(1, id);

		return query.getResultList();
	}

	public List<Comment> getCommentsOnEventPaginated(Integer id, Integer page) {
		int commentsPerPage = 10;
		int limitStart = (page - 1) * commentsPerPage;
		TypedQuery<Comment> query = em.createQuery("SELECT c FROM comment c WHERE c.event.id = ?1 ORDER BY c.timestamp DESC", Comment.class);
		query.setParameter(1, id);
		query.setFirstResult(limitStart);
		query.setMaxResults(commentsPerPage);

		return query.getResultList();
	}

}
