package cbvienna.specialisation.brissetspezialisierung.util;

import java.util.regex.Pattern;

public class ValidationUtil {

	public static boolean isValidEmail(String email) {
		String mailRegex = "^[a-z0-9\\-_.]*[^\\-_.@]@[^\\-_.@][a-z0-9\\-]{2,}\\.[a-z]{2,4}$";
		return Pattern.compile(mailRegex, Pattern.CASE_INSENSITIVE)
				.matcher(email)
				.matches();
	}

	public static boolean isValidPhoneNr(String phoneNr) {
		String mailRegex = "^[\\d+/() -]+$";
		return Pattern.compile(mailRegex)
				.matcher(phoneNr)
				.matches();
	}

	public static boolean minMaxString(String string, int minLength, int maxLength) {
		return string.length() >= minLength
				&& string.length() <= maxLength;
	}
}
