package cbvienna.specialisation.brissetspezialisierung.util;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class CalendarUtil {

	private int year, month;
	private Map<String, Object> calendarFields;
	private LocalDate lastMonth, thisMonth, nextMonth;

	private int
			thisMonthFirstWeekday, thisMonthDaysTotal, totalFields,
			lastMonthDaysTotal, lastMonthDaysStart, nextMonthBracket;

	public CalendarUtil(int year, int month) {
		this.year = year;
		this.month = month;

		this.thisMonth = LocalDate.of(year, month, 1);
		this.lastMonth = thisMonth.minusMonths(1);
		this.nextMonth = thisMonth.plusMonths(1);

		this.thisMonthFirstWeekday = thisMonth.getDayOfWeek().getValue() - 1;	// returns 0 for Monday ... 6 for Sunday
		this.thisMonthDaysTotal = thisMonth.lengthOfMonth();

		this.totalFields = (int) (Math.ceil((double) (thisMonthDaysTotal + thisMonthFirstWeekday) / 7) * 7);

		this.lastMonthDaysTotal = lastMonth.lengthOfMonth();
		this.lastMonthDaysStart = lastMonthDaysTotal - thisMonthFirstWeekday + 1;
		this.nextMonthBracket = totalFields - thisMonthFirstWeekday - thisMonthDaysTotal;
	}

	public Map<Integer, Object> calcBracketLastMonth() {
		Map<Integer, Object> bracketLastMonth = new HashMap<>();

		for (int i = lastMonthDaysStart; i <= lastMonthDaysTotal; i++) {
			bracketLastMonth.put(i, new HashMap<>());
		}

		return bracketLastMonth;
	}

	public Map<Integer, Object> calcBracketThisMonth() {
		Map<Integer, Object> bracketThisMonth = new HashMap<>();

		for (int i = 1; i <= thisMonthDaysTotal; i++) {
			bracketThisMonth.put(i, new HashMap<>());
		}

		return bracketThisMonth;
	}

	public Map<Integer, Object> calcBracketNextMonth() {
		Map<Integer, Object> bracketNextMonth = new HashMap<>();

		for (int i = 1; i <= nextMonthBracket; i++) {
			bracketNextMonth.put(i, new HashMap<>());
		}

		return bracketNextMonth;
	}

	public int getLastMonthValue() {
		return lastMonth.getMonthValue();
	}

	public int getLastMonthYear() {
		return lastMonth.getYear();
	}

	public int getNextMonthValue() {
		return nextMonth.getMonthValue();
	}

	public int getNextMonthYear() {
		return nextMonth.getYear();
	}

	/**
	 * GETTERS AND SETTERS
	 */

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public Map<String, Object> getCalendarFields() {
		return calendarFields;
	}

	public void setCalendarFields(Map<String, Object> calendarFields) {
		this.calendarFields = calendarFields;
	}

	public LocalDate getLastMonth() {
		return lastMonth;
	}

	public void setLastMonth(LocalDate lastMonth) {
		this.lastMonth = lastMonth;
	}

	public LocalDate getThisMonth() {
		return thisMonth;
	}

	public void setThisMonth(LocalDate thisMonth) {
		this.thisMonth = thisMonth;
	}

	public LocalDate getNextMonth() {
		return nextMonth;
	}

	public void setNextMonth(LocalDate nextMonth) {
		this.nextMonth = nextMonth;
	}

	public int getThisMonthFirstWeekday() {
		return thisMonthFirstWeekday;
	}

	public void setThisMonthFirstWeekday(int thisMonthFirstWeekday) {
		this.thisMonthFirstWeekday = thisMonthFirstWeekday;
	}

	public int getThisMonthDaysTotal() {
		return thisMonthDaysTotal;
	}

	public void setThisMonthDaysTotal(int thisMonthDaysTotal) {
		this.thisMonthDaysTotal = thisMonthDaysTotal;
	}

	public int getTotalFields() {
		return totalFields;
	}

	public void setTotalFields(int totalFields) {
		this.totalFields = totalFields;
	}

	public int getLastMonthDaysTotal() {
		return lastMonthDaysTotal;
	}

	public void setLastMonthDaysTotal(int lastMonthDaysTotal) {
		this.lastMonthDaysTotal = lastMonthDaysTotal;
	}

	public int getLastMonthDaysStart() {
		return lastMonthDaysStart;
	}

	public void setLastMonthDaysStart(int lastMonthDaysStart) {
		this.lastMonthDaysStart = lastMonthDaysStart;
	}

	public int getNextMonthBracket() {
		return nextMonthBracket;
	}

	public void setNextMonthBracket(int nextMonthBracket) {
		this.nextMonthBracket = nextMonthBracket;
	}
}
