package cbvienna.specialisation.brissetspezialisierung;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

/*
@SpringBootApplication
public class BrissetSpezialisierungApplication {

	public static void main(String[] args) {
		SpringApplication.run(BrissetSpezialisierungApplication.class, args);
	}

}
*/


@SpringBootApplication
@ComponentScan("cbvienna.specialisation.brissetspezialisierung")
public class BrissetSpezialisierungApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(BrissetSpezialisierungApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(BrissetSpezialisierungApplication.class, args);
	}

}
