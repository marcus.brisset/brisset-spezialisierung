package cbvienna.specialisation.brissetspezialisierung.securityconfiguration;

import cbvienna.specialisation.brissetspezialisierung.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class MyAuthenticationManager implements AuthenticationManager {

	@Autowired
	MyUserDetailsService myUserDetailsService;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		String password = userRepository.getUserByEmail(authentication.getName()).getPassword();
		if (passwordEncoder.matches((String) authentication.getCredentials(), password)) {
			var token = new UsernamePasswordAuthenticationToken(
					authentication.getName(),
					authentication.getCredentials(),
					Arrays.asList(myUserDetailsService.getAuthority(userRepository.getUserByEmail(authentication.getName()).getType()))
			);

			return token;
		}

		throw new BadCredentialsException("bad credentials");
	}


}
