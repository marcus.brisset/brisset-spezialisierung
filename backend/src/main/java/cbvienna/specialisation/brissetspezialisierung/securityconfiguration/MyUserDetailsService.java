package cbvienna.specialisation.brissetspezialisierung.securityconfiguration;

import cbvienna.specialisation.brissetspezialisierung.model.User;
import cbvienna.specialisation.brissetspezialisierung.model.UserType;
import cbvienna.specialisation.brissetspezialisierung.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MyUserDetailsService implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		User user = userRepository.getUserByEmail(email);

		if(user == null) {
			throw new UsernameNotFoundException(email + " does not exist.");
		}

		var list = getAuthority(user.getType());

		UserDetails springUser = org.springframework.security.core.userdetails.User.withUsername(user.getEmail()).password(user.getPassword())
				.authorities(list).build();
		return springUser;
	}

	public GrantedAuthority[] getAuthority (UserType userType) {
		List<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority(userType.toString()));

		return  authorities.toArray(new GrantedAuthority[0]);
	}

}
