package cbvienna.specialisation.brissetspezialisierung.dto;

public class AttendedEventsDTO {

	private int userId, eventId;

	public AttendedEventsDTO() {}

	public AttendedEventsDTO(int userId, int eventId) {
		this.userId = userId;
		this.eventId = eventId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

}
