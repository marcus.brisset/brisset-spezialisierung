package cbvienna.specialisation.brissetspezialisierung.dto;

import java.util.List;

public class PaginatedContentDTO<T> {

    private int totalPages;
    private List<T> paginatedContent;

    public PaginatedContentDTO() {}

    public PaginatedContentDTO(int totalPages, List<T> paginatedContent) {
        this.totalPages = totalPages;
        this.paginatedContent = paginatedContent;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public List<T> getPaginatedContent() {
        return paginatedContent;
    }

    public void setPaginatedContent(List<T> paginatedContent) {
        this.paginatedContent = paginatedContent;
    }
}
