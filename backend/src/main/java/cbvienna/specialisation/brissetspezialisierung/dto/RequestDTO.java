package cbvienna.specialisation.brissetspezialisierung.dto;

import cbvienna.specialisation.brissetspezialisierung.model.Request;

import java.sql.Date;
import java.sql.Timestamp;

public class RequestDTO {

	private int id;
	private Timestamp timestamp;
	private Date eventDateStart, eventDateEnd;
	private String name, email, phoneNr, message;
	private boolean isNew;

	public RequestDTO() {}

	public RequestDTO(Request request) {
		this.id = request.getId();
		this.timestamp = request.getTimestamp();
		this.eventDateStart = request.getEventDateStart();
		this.eventDateEnd = request.getEventDateEnd();
		this.name = request.getName();
		this.email = request.getEmail();
		this.phoneNr = request.getPhoneNr();
		this.message = request.getMessage();
		this.isNew = request.isNew();
	}

	public Request toRequest() {
		Request request = new Request();

		request.setEventDateStart(this.eventDateStart);
		request.setEventDateEnd(this.eventDateEnd);
		request.setName(this.name);
		request.setEmail(this.email);
		request.setPhoneNr(this.phoneNr);
		request.setMessage(this.message);
		request.setNew(this.isNew);

		return request;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public Date getEventDateStart() {
		return eventDateStart;
	}

	public void setEventDateStart(Date eventDateStart) {
		this.eventDateStart = eventDateStart;
	}

	public Date getEventDateEnd() {
		return eventDateEnd;
	}

	public void setEventDateEnd(Date eventDateEnd) {
		this.eventDateEnd = eventDateEnd;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNr() {
		return phoneNr;
	}

	public void setPhoneNr(String phoneNr) {
		this.phoneNr = phoneNr;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean aNew) {
		isNew = aNew;
	}
}
