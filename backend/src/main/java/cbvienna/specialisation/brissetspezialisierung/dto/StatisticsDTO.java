package cbvienna.specialisation.brissetspezialisierung.dto;

public class StatisticsDTO {

	private int totalUsers;
	private int totalEventTypes;
	private int totalEvents;
	private int totalComments;
	private int totalRequests;
	private int totalAttendees;

	public StatisticsDTO() {}

	public StatisticsDTO(
			int totalUsers,
			int totalEventTypes,
			int totalEvents,
			int totalComments,
			int totalRequests,
			int totalAttendees
	) {
		this.totalUsers = totalUsers;
		this.totalEventTypes = totalEventTypes;
		this.totalEvents = totalEvents;
		this.totalComments = totalComments;
		this.totalRequests = totalRequests;
		this.totalAttendees = totalAttendees;
	}

	public int getTotalUsers() {
		return totalUsers;
	}

	public void setTotalUsers(int totalUsers) {
		this.totalUsers = totalUsers;
	}

	public int getTotalEventTypes() {
		return totalEventTypes;
	}

	public void setTotalEventTypes(int totalEventTypes) {
		this.totalEventTypes = totalEventTypes;
	}

	public int getTotalEvents() {
		return totalEvents;
	}

	public void setTotalEvents(int totalEvents) {
		this.totalEvents = totalEvents;
	}

	public int getTotalComments() {
		return totalComments;
	}

	public void setTotalComments(int totalComments) {
		this.totalComments = totalComments;
	}

	public int getTotalRequests() {
		return totalRequests;
	}

	public void setTotalRequests(int totalRequests) {
		this.totalRequests = totalRequests;
	}

	public int getTotalAttendees() {
		return totalAttendees;
	}

	public void setTotalAttendees(int totalAttendees) {
		this.totalAttendees = totalAttendees;
	}

}
