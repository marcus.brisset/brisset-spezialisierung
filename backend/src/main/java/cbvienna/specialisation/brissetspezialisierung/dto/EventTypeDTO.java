package cbvienna.specialisation.brissetspezialisierung.dto;

import cbvienna.specialisation.brissetspezialisierung.model.EventType;

public class EventTypeDTO {

	private int id, totalEventsOfType;
	private String title;

	public EventTypeDTO() {}

	public EventTypeDTO(EventType eventType) {
		this.id = eventType.getId();
		this.title = eventType.getTitle();
	}

	public EventType toEventType() {
		EventType eventType = new EventType();

		eventType.setId(this.id);
		eventType.setTitle(this.title);

		return eventType;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getTotalEventsOfType() {
		return totalEventsOfType;
	}

	public void setTotalEventsOfType(int totalEventsOfType) {
		this.totalEventsOfType = totalEventsOfType;
	}

}
