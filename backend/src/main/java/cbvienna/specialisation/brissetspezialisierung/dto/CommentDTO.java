package cbvienna.specialisation.brissetspezialisierung.dto;

import cbvienna.specialisation.brissetspezialisierung.model.Comment;
import cbvienna.specialisation.brissetspezialisierung.model.Event;
import cbvienna.specialisation.brissetspezialisierung.model.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.sql.Timestamp;

public class CommentDTO {

	private int id;
	private Timestamp timestamp;
	@JsonIgnoreProperties({"eventType", "attendsEvent", "commentSet"})
	private Event event;
	@JsonIgnoreProperties({"type", "password", "email", "requestSet", "attendedEvents", "commentSet"})
	private User user;
	private String text;

	public CommentDTO() {}

	public CommentDTO(Comment comment) {
		this.id = comment.getId();
		this.timestamp = comment.getTimestamp();
		this.event = comment.getEvent();
		this.user = comment.getUser();
		this.text = comment.getText();
	}

	public Comment toComment() {
		Comment comment = new Comment();

		comment.setId(this.id);
		comment.setEvent(this.event);
		comment.setUser(this.user);
		comment.setText(this.text);

		return comment;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
