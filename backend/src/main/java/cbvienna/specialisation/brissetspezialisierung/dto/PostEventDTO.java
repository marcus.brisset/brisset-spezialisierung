package cbvienna.specialisation.brissetspezialisierung.dto;

import java.sql.Date;
import java.sql.Time;

public class PostEventDTO {

	private int id;
	private int eventTypeId;
	private String title;
	private Date dateStart;
	private Date dateEnd;
	private double price;
	private Time doorsOpen;
	private String image;
	private String text;

	public PostEventDTO() {}

	public PostEventDTO(int id, int eventTypeId, String title, Date dateStart, Date dateEnd, double price, Time doorsOpen, String image, String text) {
		this.id = id;
		this.eventTypeId = eventTypeId;
		this.title = title;
		this.dateStart = dateStart;
		this.dateEnd = dateEnd;
		this.price = price;
		this.doorsOpen = doorsOpen;
		this.image = image;
		this.text = text;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEventTypeId() {
		return eventTypeId;
	}

	public void setEventTypeId(int eventTypeId) {
		this.eventTypeId = eventTypeId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getDateStart() {
		return dateStart;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}

	public Date getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Time getDoorsOpen() {
		return doorsOpen;
	}

	public void setDoorsOpen(Time doorsOpen) {
		this.doorsOpen = doorsOpen;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
