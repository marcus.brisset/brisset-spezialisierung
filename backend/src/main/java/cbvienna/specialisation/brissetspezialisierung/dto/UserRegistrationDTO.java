package cbvienna.specialisation.brissetspezialisierung.dto;

import cbvienna.specialisation.brissetspezialisierung.model.User;
import cbvienna.specialisation.brissetspezialisierung.model.UserType;

public class UserRegistrationDTO {

	private String name, email, password, passwordRepeat;
	private UserType type;

	public UserRegistrationDTO() {
	}

	public UserRegistrationDTO(String name, String email, String password, String passwordRepeat) {
		this.name = name;
		this.email = email;
		this.password = password;
		this.passwordRepeat = passwordRepeat;
		this.type = UserType.USER;
	}

	public User toUser() {
		User user = new User();

		user.setName(this.name);
		user.setEmail(this.email);
		user.setPassword(this.password);
		user.setType(this.type);

		return user;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordRepeat() {
		return passwordRepeat;
	}

	public void setPasswordRepeat(String passwordRepeat) {
		this.passwordRepeat = passwordRepeat;
	}

	public UserType getType() {
		return type;
	}

	public void setType(UserType type) {
		this.type = type;
	}
}
