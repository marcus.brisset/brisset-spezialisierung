package cbvienna.specialisation.brissetspezialisierung.dto;

public class PostCommentDTO {

	private int eventId, userId;
	private String comment;

	public PostCommentDTO() {
	}

	public PostCommentDTO(int eventId, int userId, String comment) {
		this.eventId = eventId;
		this.userId = userId;
		this.comment = comment;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
}
