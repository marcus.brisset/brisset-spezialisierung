package cbvienna.specialisation.brissetspezialisierung.dto;

import cbvienna.specialisation.brissetspezialisierung.model.User;

public class UserDTO {

	private int id, totalAttendedEvents, totalCommentsOnEvents;
	private String name, email;

	public UserDTO() {}

	public UserDTO(User user) {
		this.id = user.getId();
		this.name = user.getName();
		this.email = user.getEmail();
		this.totalAttendedEvents = user.getAttendedEvents().size();
		this.totalCommentsOnEvents = user.getCommentSet().size();
	}

 	public User toUser() {
		User user = new User();

		user.setId(this.id);
		user.setName(this.name);
		user.setEmail(this.email);

		return user;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getTotalAttendedEvents() {
		return totalAttendedEvents;
	}

	public void setTotalAttendedEvents(int totalAttendedEvents) {
		this.totalAttendedEvents = totalAttendedEvents;
	}

	public int getTotalCommentsOnEvents() {
		return totalCommentsOnEvents;
	}

	public void setTotalCommentsOnEvents(int totalCommentsOnEvents) {
		this.totalCommentsOnEvents = totalCommentsOnEvents;
	}
}
