package cbvienna.specialisation.brissetspezialisierung.dto;

import cbvienna.specialisation.brissetspezialisierung.model.Event;
import cbvienna.specialisation.brissetspezialisierung.model.EventType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class EventDTO {

	private int id;
	private Timestamp timestamp;
	private String
			title, image, text;
	private Date dateStart, dateEnd;
	private double price;
	private Time doorsOpen;
	private int totalCommentsOnEvent, totalUsersAttendingEvent;
	private int eventDurationInDays;

	@JsonIgnoreProperties("eventSet")	// prevents infinite loop
	private EventType eventType;

	public EventDTO() {}

	public EventDTO(Event event) {
		this.id = event.getId();
		this.timestamp = event.getTimestamp();
		this.title = event.getTitle();
		this.dateStart = event.getDateStart();
		this.dateEnd = event.getDateEnd();
		this.price = event.getPrice();
		this.doorsOpen = event.getDoorsOpen();
		this.image = event.getImage();
		this.text = event.getText();
		this.eventType = event.getEventType();
		this.eventDurationInDays = calcEventDurationInDays();
	}

	public Event toEvent() {
		Event event = new Event();

		event.setId(this.id);
		event.setTimestamp(this.timestamp);
		event.setTitle(this.title);
		event.setDateStart(this.dateStart);
		event.setDateEnd(this.dateEnd);
		event.setPrice(this.price);
		event.setDoorsOpen(this.doorsOpen);
		event.setImage(this.image);
		event.setText(this.text);
		event.setEventType(this.eventType);

		return event;
	}

	public EventShortDTO toShortDTO() {
		return new EventShortDTO(id, title, image);
	}

	public int getStartDay() {
		return dateStart.toLocalDate().getDayOfMonth();
	}

	public int getStartMonth() {
		return dateStart.toLocalDate().getMonthValue();
	}

	public int getEndDay() {
		return dateEnd.toLocalDate().getDayOfMonth();
	}

	public int calcEventDurationInDays() {
		LocalDate start = this.dateStart.toLocalDate();
		LocalDate end = this.dateEnd.toLocalDate();
		return (int) ChronoUnit.DAYS.between(start, end);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getDateStart() {
		return dateStart;
	}

	public void setDateStart(Date dateStart) {
		this.dateStart = dateStart;
	}

	public Date getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(Date dateEnd) {
		this.dateEnd = dateEnd;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Time getDoorsOpen() {
		return doorsOpen;
	}

	public void setDoorsOpen(Time doorsOpen) {
		this.doorsOpen = doorsOpen;
	}

	public EventType getEventType() {
		return eventType;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}

	public int getTotalCommentsOnEvent() {
		return totalCommentsOnEvent;
	}

	public void setTotalCommentsOnEvent(int totalCommentsOnEvent) {
		this.totalCommentsOnEvent = totalCommentsOnEvent;
	}

	public int getTotalUsersAttendingEvent() {
		return totalUsersAttendingEvent;
	}

	public void setTotalUsersAttendingEvent(int totalUsersAttendingEvent) {
		this.totalUsersAttendingEvent = totalUsersAttendingEvent;
	}

	public int getEventDurationInDays() {
		return eventDurationInDays;
	}

	public void setEventDurationInDays(int eventDurationInDays) {
		this.eventDurationInDays = eventDurationInDays;
	}
}
