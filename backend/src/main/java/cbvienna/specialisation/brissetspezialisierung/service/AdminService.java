package cbvienna.specialisation.brissetspezialisierung.service;

import cbvienna.specialisation.brissetspezialisierung.dto.StatisticsDTO;
import cbvienna.specialisation.brissetspezialisierung.model.*;
import cbvienna.specialisation.brissetspezialisierung.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    EventTypeRepository eventTypeRepository;

    @Autowired
    EventRepository eventRepository;

    @Autowired
    CommentRepository commentRepository;

    @Autowired
    RequestRepository requestRepository;

    @Autowired
    AttendedEventsService attendedEventsService;

    public StatisticsDTO getStatistics() {
        List<User> userList = userRepository.getUsers();
        List<EventType> eventTypeList = eventTypeRepository.getEventTypes();
        List<Event> eventList = eventRepository.getEvents();
        List<Comment> commentList = commentRepository.getComments();
        List<Request> requestList = requestRepository.getRequests();

        int totalUsers = userList != null ? userList.size() : 0;
        int totalEventTypes = eventTypeList != null ? eventTypeList.size() : 0;
        int totalEvents = eventList != null ? eventList.size() : 0;
        int totalComments = commentList != null ? commentList.size() : 0;
        int totalRequests = requestList != null ? requestList.size() : 0;

        int totalAttendees = 0;
        if (eventList != null) {
            for (Event event : eventList) {
                totalAttendees += event.getAttendsEvent().size();
            }
        }

        return new StatisticsDTO(
                totalUsers, totalEventTypes, totalEvents, totalComments, totalRequests, totalAttendees
        );
    }
}