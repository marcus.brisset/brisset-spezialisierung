package cbvienna.specialisation.brissetspezialisierung.service;

import cbvienna.specialisation.brissetspezialisierung.dto.AttendedEventsDTO;
import cbvienna.specialisation.brissetspezialisierung.model.Event;
import cbvienna.specialisation.brissetspezialisierung.model.User;
import cbvienna.specialisation.brissetspezialisierung.repository.EventRepository;
import cbvienna.specialisation.brissetspezialisierung.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class AttendedEventsService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	EventRepository eventRepository;

/*
	POST
*/

	public ResponseEntity<String> addAttendedEvent(AttendedEventsDTO attendedEventsDTO) {
		User user = userRepository.getUserById(attendedEventsDTO.getUserId());
		Event event = eventRepository.getEventById(attendedEventsDTO.getEventId());

		if (user == null || event == null) {
			return null;
		}

		user.addAttendedEvent(event);
		event.addAttendsEvent(user);
		userRepository.updateUserData(user);
		eventRepository.updateEventData(event);
		return new ResponseEntity<>("success", HttpStatus.OK);
	}

	public ResponseEntity<String> removeAttendedEvent(AttendedEventsDTO attendedEventsDTO) {
		User user = userRepository.getUserById(attendedEventsDTO.getUserId());
		Event event = eventRepository.getEventById(attendedEventsDTO.getEventId());

		if (user == null || event == null) {
			return null;
		}

		user.removeAttendedEvent(event);
		event.removeAttendsEvent(user);
		userRepository.updateUserData(user);
		eventRepository.updateEventData(event);
		return new ResponseEntity<>("success", HttpStatus.OK);
	}

/*
	GET
*/

	public ResponseEntity<Boolean> checkAttendance(Integer userId, Integer eventId) {
		boolean isAttending = userRepository.checkAttendance(userId, eventId);

		return new ResponseEntity<>(isAttending, HttpStatus.OK);
	}

}
