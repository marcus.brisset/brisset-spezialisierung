package cbvienna.specialisation.brissetspezialisierung.service;

import cbvienna.specialisation.brissetspezialisierung.dto.EventTypeDTO;
import cbvienna.specialisation.brissetspezialisierung.model.EventType;
import cbvienna.specialisation.brissetspezialisierung.repository.EventTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EventTypeService {

	@Autowired
	EventTypeRepository eventTypeRepository;

/*
	POST
*/

	public ResponseEntity<String> addEventType(EventTypeDTO eventTypeDTO) {
		eventTypeRepository.addEventType(eventTypeDTO.toEventType());

		return new ResponseEntity<>( "success", HttpStatus.OK);
	}

/*
	PUT
*/

	public ResponseEntity<String> updateEventTypeData(EventTypeDTO eventTypeDTO) {
		EventType eventType = eventTypeRepository.getEventTypeById(eventTypeDTO.getId());

		if (eventType == null) {
			return new ResponseEntity<>("not found", HttpStatus.NOT_FOUND);
		}

		eventType.setTitle(eventTypeDTO.getTitle());

		eventTypeRepository.updateEventTypeData(eventType);

		return new ResponseEntity<>("success", HttpStatus.OK);
	}

/*
	DELETE
 */

	public ResponseEntity<String> deleteEventType(Integer id) {
		EventType eventType = eventTypeRepository.getEventTypeById(id);

		if (eventType == null) {
			return new ResponseEntity<>("not found", HttpStatus.NOT_FOUND);
		}

		eventTypeRepository.deleteEventType(eventType);

		return new ResponseEntity<>("success", HttpStatus.OK);
	}

/*
	GET
*/

	public List<EventTypeDTO> getEventTypes() {
		List<EventTypeDTO> eventTypeDTOList = new ArrayList<>();
		List<EventType> eventTypeList = eventTypeRepository.getEventTypes();

		if (eventTypeList == null) {
			return null;
		}

		for (EventType eventType : eventTypeList) {
			eventTypeDTOList.add(eventType.toEventTypeDTO());
		}

		return eventTypeDTOList;
	}

	public EventTypeDTO getEventTypeById(Integer id) {
		EventType eventType = eventTypeRepository.getEventTypeById(id);

		if (eventType == null) {
			return null;
		}

		return eventTypeRepository.getEventTypeById(id).toEventTypeDTO();
	}

	public List<EventTypeDTO> getEventTypesByTitle(String title) {
		List<EventTypeDTO> eventTypeDTOList = new ArrayList<>();
		List<EventType> eventTypeList = eventTypeRepository.getEventTypesByTitle(title);

		for (EventType eventType : eventTypeList) {
			eventTypeDTOList.add(eventType.toEventTypeDTO());
		}

		return eventTypeDTOList;
	}

}
