package cbvienna.specialisation.brissetspezialisierung.service;

import cbvienna.specialisation.brissetspezialisierung.dto.PaginatedContentDTO;
import cbvienna.specialisation.brissetspezialisierung.dto.UserDTO;
import cbvienna.specialisation.brissetspezialisierung.dto.UserRegistrationDTO;
import cbvienna.specialisation.brissetspezialisierung.model.Event;
import cbvienna.specialisation.brissetspezialisierung.model.User;
import cbvienna.specialisation.brissetspezialisierung.model.UserType;
import cbvienna.specialisation.brissetspezialisierung.repository.EventRepository;
import cbvienna.specialisation.brissetspezialisierung.repository.UserRepository;
import cbvienna.specialisation.brissetspezialisierung.util.ValidationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class UserService {

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	UserRepository userRepository;

	@Autowired
	EventRepository eventRepository;

/*
	POST
*/

	public ResponseEntity<String> addUser(UserRegistrationDTO userRegistrationDTO) {
		boolean isVeryFirstUser = userRepository.getAllAccounts() == null;

		// Check if any of the DTOs properties (except type) is empty
		AtomicBoolean valid = new AtomicBoolean(true);
		ReflectionUtils.doWithFields(userRegistrationDTO.getClass(), field -> {
			field.setAccessible(true);
			if (
					!field.getName().equals("type")
							&& (field.get(userRegistrationDTO) == null || field.get(userRegistrationDTO).toString().trim().equals(""))
			) {
				valid.set(false);
			}
		});
		if (!valid.get()) {
			return new ResponseEntity<>("missing_input", HttpStatus.BAD_REQUEST);
		}

		// Check if the e-mail has a valid pattern (passed all my tests so far)
		if (!ValidationUtil.isValidEmail(userRegistrationDTO.getEmail())) {
			return new ResponseEntity<>("invalid_mail_pattern", HttpStatus.BAD_REQUEST);
		}

		// Check if name length is within limits
		if (!ValidationUtil.minMaxString(userRegistrationDTO.getName(), 3, 15)) {
			return new ResponseEntity<>("user_name_length", HttpStatus.BAD_REQUEST);
		}

		// Check if passwords match
		if (!userRegistrationDTO.getPassword().equals(userRegistrationDTO.getPasswordRepeat())) {
			return new ResponseEntity<>("password_mismatch", HttpStatus.BAD_REQUEST);
		}

		// First registered User gets to be admin
		userRegistrationDTO.setPassword(passwordEncoder.encode(userRegistrationDTO.getPassword()));
		userRegistrationDTO.setType(isVeryFirstUser ?
				UserType.ADMIN :
				UserType.USER);

		userRepository.addUser(userRegistrationDTO.toUser());

		return new ResponseEntity<>("success", HttpStatus.CREATED);
	}

/*
	PUT
*/

	public ResponseEntity<String> updateUserData(UserDTO userDTO) {
		User user = userRepository.getUserById(userDTO.getId());

		if (user == null) {
			return new ResponseEntity<>("not found", HttpStatus.NOT_FOUND);
		}

		user.setName(userDTO.getName());
		userRepository.updateUserData(user);

		return new ResponseEntity<>("success", HttpStatus.OK);
	}

/*
	DELETE
 */

	public ResponseEntity<String> deleteUser(Integer id) {
		User user = userRepository.getUserById(id);

		if (user == null) {
			return new ResponseEntity<>("not found", HttpStatus.NOT_FOUND);
		}

		Set<Event> eventSet = user.getAttendedEvents();
		for (Event event : eventSet) {
			event.removeAttendsEvent(user);
			eventRepository.updateEventData(event);
		}
		userRepository.deleteUser(user);

		return new ResponseEntity<>("success", HttpStatus.OK);
	}

/*
	GET
*/

	public List<UserDTO> getUsers() {
		List<UserDTO> userDTOList = new ArrayList<>();
		List<User> userList = userRepository.getUsers();

		if (userList == null) {
			return null;
		}

		for (User user : userList) {
			userDTOList.add(user.toUserDTO());
		}

		return userDTOList;
	}

	public PaginatedContentDTO<UserDTO> getUsersPaginated(Integer page) {
		int usersPerPage = 10;
		List<User> userList = userRepository.getUsers();

		if (userList == null) {
			return null;
		}

		int totalUsers = userList.size();
		int totalPages = (int) Math.ceil((float) totalUsers / usersPerPage);
		int setPage = (page == null || page > totalPages || page < 0) ? 1 : page;

		List<User> userListPaginated = userRepository.getUsersPaginated(setPage);
		List<UserDTO> userDTOListPaginated = new ArrayList<>();
		for (User user : userListPaginated) {
			userDTOListPaginated.add(user.toUserDTO());
		}

		return new PaginatedContentDTO<>(totalPages, userDTOListPaginated);
	}

	public UserDTO getUserById(Integer id) {
		User user = userRepository.getUserById(id);

		if (user == null) {
			return null;
		}

		return userRepository.getUserById(id).toUserDTO();
	}

	public List<UserDTO> getUsersByName(String name) {
		List<UserDTO> userDTOList = new ArrayList<>();
		List<User> userList = userRepository.getUsersByName(name);

		for (User user : userList) {
			userDTOList.add(user.toUserDTO());
		}

		return userDTOList;
	}

}
