package cbvienna.specialisation.brissetspezialisierung.service;

import cbvienna.specialisation.brissetspezialisierung.dto.CommentDTO;
import cbvienna.specialisation.brissetspezialisierung.dto.PaginatedContentDTO;
import cbvienna.specialisation.brissetspezialisierung.model.Comment;
import cbvienna.specialisation.brissetspezialisierung.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CommentService {

	@Autowired
	CommentRepository commentRepository;

/*
	POST
*/

	public ResponseEntity<String> addComment(CommentDTO commentDTO) {
		if (commentDTO.getText() == null || commentDTO.getText().equals("")) {
			return new ResponseEntity<>("missing fields", HttpStatus.NO_CONTENT);
		}

		commentRepository.addComment(commentDTO.toComment());

		return new ResponseEntity<>("success", HttpStatus.OK);
	}

/*
	PUT
*/

	public ResponseEntity<String> updateCommentData(CommentDTO commentDTO) {
		if (commentRepository.getCommentById(commentDTO.getId()) == null) {
			return new ResponseEntity<>("not found", HttpStatus.NOT_FOUND);
		}

		commentRepository.updateCommentData(commentDTO.toComment());

		return new ResponseEntity<>("success", HttpStatus.OK);
	}

/*
	DELETE
 */

	public ResponseEntity<String> deleteComment(Integer id) {
		if (commentRepository.getCommentById(id) == null) {
			return new ResponseEntity<>("not found", HttpStatus.NOT_FOUND);
		}

		commentRepository.deleteComment(commentRepository.getCommentById(id));

		return new ResponseEntity<>("success", HttpStatus.OK);
	}

/*
	GET
*/

	public List<CommentDTO> getComments() {
		if (commentRepository.getComments() == null) {
			return null;
		}

		List<CommentDTO> commentDTOList = new ArrayList<>();
		for (int i = 0; i < commentRepository.getComments().size(); i++) {
			commentDTOList.add(commentRepository.getComments().get(i).toCommentDTO());
		}

		return commentDTOList;
	}

	public CommentDTO getCommentById(Integer id) {
		if (commentRepository.getCommentById(id) == null) {
			return null;
		}

		return commentRepository.getCommentById(id).toCommentDTO();
	}

	public List<CommentDTO> getCommentsOnEvent(Integer id) {
		if (commentRepository.getCommentsOnEvent(id) == null) {
			return null;
		}

		List<CommentDTO> commentDTOList = new ArrayList<>();
		for (int i = 0; i < commentRepository.getCommentsOnEvent(id).size(); i++) {
			commentDTOList.add(commentRepository.getCommentsOnEvent(id).get(i).toCommentDTO());
		}

		return commentDTOList;
	}

	public PaginatedContentDTO<CommentDTO> getCommentsOnEventPaginated(Integer id, Integer page) {
		int commentsPerPage = 10;
		List<Comment> commentList = commentRepository.getCommentsOnEvent(id);

		if (commentList == null) {
			return null;
		}

		int totalCommentsOnEvent = commentList.size();
		int totalPages = (int) Math.ceil((float) totalCommentsOnEvent / commentsPerPage);
		int setPage = (page == null || page > totalPages || page < 0) ? 1 : page;

		List<Comment> commentListPaginated = commentRepository.getCommentsOnEventPaginated(id, setPage);
		List<CommentDTO> commentDTOListPaginated = new ArrayList<>();
		for (Comment comment : commentListPaginated) {
			commentDTOListPaginated.add(comment.toCommentDTO());
		}

		return new PaginatedContentDTO<>(totalPages, commentDTOListPaginated);
	}

}
