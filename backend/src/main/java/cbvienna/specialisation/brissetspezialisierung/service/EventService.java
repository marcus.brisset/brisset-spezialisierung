package cbvienna.specialisation.brissetspezialisierung.service;

import cbvienna.specialisation.brissetspezialisierung.dto.*;
import cbvienna.specialisation.brissetspezialisierung.model.Event;
import cbvienna.specialisation.brissetspezialisierung.model.EventType;
import cbvienna.specialisation.brissetspezialisierung.repository.EventRepository;
import cbvienna.specialisation.brissetspezialisierung.repository.EventTypeRepository;
import cbvienna.specialisation.brissetspezialisierung.util.CalendarUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class EventService {

	@Autowired
	EventRepository eventRepository;

	@Autowired
	EventTypeRepository eventTypeRepository;

/*
	POST
*/

	public ResponseEntity<String> addEvent(EventDTO eventDTO) {
		if (
				eventDTO.getTitle().equals("")
						|| eventDTO.getPrice() < 0
						|| eventDTO.getImage().equals("")
						|| eventDTO.getText().equals("")
		) {
			return new ResponseEntity<>("missing fields", HttpStatus.NO_CONTENT);
		}

		if (eventDTO.getDateStart().after(eventDTO.getDateEnd())) {
			return new ResponseEntity<>("start after end", HttpStatus.NOT_ACCEPTABLE);
		}

		if (eventRepository.dateNotAvailable(eventDTO.getDateStart(), eventDTO.getDateEnd())) {
			return new ResponseEntity<>("not available", HttpStatus.ACCEPTED);
		}

		eventRepository.addEvent(eventDTO.toEvent());

		return new ResponseEntity<>("success", HttpStatus.CREATED);
	}


/*
	PUT
*/

	public ResponseEntity<String> updateEventData(PostEventDTO postEventDTO) {
		Event event = eventRepository.getEventById(postEventDTO.getId());
		EventType eventType = eventTypeRepository.getEventTypeById(postEventDTO.getEventTypeId());

		if (event == null) {
			return new ResponseEntity<>("event not found", HttpStatus.NOT_FOUND);
		}

		if (eventType == null) {
			return new ResponseEntity<>("event type not found", HttpStatus.NOT_FOUND);
		}

		if (eventRepository.dateNotAvailable(postEventDTO.getDateStart(), postEventDTO.getDateEnd(), event.getId())) {
			return new ResponseEntity<>("not available", HttpStatus.ACCEPTED);
		}

		event.setEventType(eventType);
		event.setTitle(postEventDTO.getTitle());
		event.setDateStart(postEventDTO.getDateStart());
		event.setDateEnd(postEventDTO.getDateEnd());
		event.setPrice(postEventDTO.getPrice());
		event.setDoorsOpen(postEventDTO.getDoorsOpen());
		event.setImage(postEventDTO.getImage());
		event.setText(postEventDTO.getText());

		eventRepository.updateEventData(event);

		return new ResponseEntity<>("success", HttpStatus.OK);
	}

/*
	DELETE
 */

	public ResponseEntity<String> deleteEvent(Integer id) {
		Event event = eventRepository.getEventById(id);

		if (event == null) {
			return new ResponseEntity<>("not found", HttpStatus.NOT_FOUND);
		}

		eventRepository.deleteEvent(event);

		return new ResponseEntity<>("success", HttpStatus.OK);
	}

/*
	GET
*/

	public List<EventDTO> getEvents() {
		List<EventDTO> eventDTOList = new ArrayList<>();
		List<Event> eventList = eventRepository.getEvents();

		if (eventList == null) {
			return null;
		}

		for (Event event : eventList) {
			eventDTOList.add(event.toEventDTO());
		}

		return eventDTOList;
	}

	public PaginatedContentDTO<EventDTO> getEventsPaginated(Integer page) {
		int eventsPerPage = 10;
		List<Event> eventList = eventRepository.getEvents();

		if (eventList == null) {
			return null;
		}

		int totalEvents = eventList.size();
		int totalPages = (int) Math.ceil((float) totalEvents / eventsPerPage);
		int setPage = (page == null || page > totalPages || page < 0) ? 1 : page;

		List<Event> eventListPaginated = eventRepository.getEventsPaginated(setPage);
		List<EventDTO> eventDTOListPaginated = new ArrayList<>();
		for (Event event : eventListPaginated) {
			eventDTOListPaginated.add(event.toEventDTO());
		}

		return new PaginatedContentDTO<>(totalPages, eventDTOListPaginated);
	}

	public EventDTO getEventById(Integer id) {
		Event event = eventRepository.getEventById(id);

		if (event == null) {
			return null;
		}

		return eventRepository.getEventById(id).toEventDTO();
	}

	public List<EventDTO> getEventsByTitle(String title) {
		List<EventDTO> eventDTOList = new ArrayList<>();
		List<Event> eventList = eventRepository.getEventsByTitle(title);

		for (Event event : eventList) {
			eventDTOList.add(event.toEventDTO());
		}

		return eventDTOList;
	}

	public List<EventDTO> getEventsBySearchTerm(String searchTerm) {
		List<EventDTO> eventDTOList = new ArrayList<>();
		List<Event> eventList = eventRepository.getEventsBySearchTerm(searchTerm);

		for (Event event : eventList) {
			eventDTOList.add(event.toEventDTO());
		}

		return eventDTOList;
	}

	public List<EventDTO> getEventsByMonth(Integer year, Integer month) {
		List<EventDTO> eventDTOList = new ArrayList<>();
		List<Event> eventList = eventRepository.getEventsByMonth(year, month);

		for (Event event : eventList) {
			eventDTOList.add(event.toEventDTO());
		}

		return eventDTOList;
	}

	public Map<String, Object> getEventCalendar(Integer year, Integer month) {
		Map<String, Object> eventCalendar = new HashMap<>();
		CalendarUtil calendar = new CalendarUtil(year, month);

		Map<Integer, Object> thisMonthFields = calendar.calcBracketThisMonth();

		List<EventDTO> eventDTOList = getEventsByMonth(year, month);

		if (!eventDTOList.isEmpty()) {
			for (EventDTO eventDto : eventDTOList) {
				int eventStartDay = eventDto.getStartDay();
				int eventStartMonth = eventDto.getStartMonth();
				int eventEndDay = eventDto.getEndDay();

				if (eventStartDay > eventEndDay) {
					if (eventStartMonth != month) {
						eventStartDay = 1;
					} else {
						eventEndDay = thisMonthFields.size();
					}
				}

				for (int i = eventStartDay; i <= eventEndDay; i++) {
					thisMonthFields.put(i, eventDto.toShortDTO());
				}
			}
		}

		eventCalendar.put("last", calendar.calcBracketLastMonth());
		eventCalendar.put("this", thisMonthFields);
		eventCalendar.put("next", calendar.calcBracketNextMonth());
		eventCalendar.put("navPrevMonth", calendar.getLastMonthValue());
		eventCalendar.put("navPrevYear", calendar.getLastMonthYear());
		eventCalendar.put("navNextMonth", calendar.getNextMonthValue());
		eventCalendar.put("navNextYear", calendar.getNextMonthYear());

		return eventCalendar;
	}

	public List<EventShortDTO> eventsUsingFlyer(String filename) {
		List<Event> eventList = eventRepository.getEvents();
		List<EventShortDTO> eventIds = new ArrayList<>();

		if (eventList == null) {
			return null;
		}

		for (Event event : eventList) {
			if (event.getImage().equals(filename)) {
				eventIds.add(event.toShortDTO());
			}
		}

		return eventIds;
	}

}