package cbvienna.specialisation.brissetspezialisierung.service;

import cbvienna.specialisation.brissetspezialisierung.dto.PaginatedContentDTO;
import cbvienna.specialisation.brissetspezialisierung.dto.RequestDTO;
import cbvienna.specialisation.brissetspezialisierung.model.Request;
import cbvienna.specialisation.brissetspezialisierung.repository.EventRepository;
import cbvienna.specialisation.brissetspezialisierung.repository.RequestRepository;
import cbvienna.specialisation.brissetspezialisierung.util.ValidationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RequestService {

	@Autowired
	RequestRepository requestRepository;

	@Autowired
	EventRepository eventRepository;

/*
	POST
*/

	public ResponseEntity<String> addRequest(RequestDTO requestDTO) {
		if (
				requestDTO.getEventDateStart() == null || requestDTO.getEventDateStart().toString().equals("")
						|| requestDTO.getEventDateEnd() == null || requestDTO.getEventDateEnd().toString().equals("")
						|| requestDTO.getName() == null || requestDTO.getName().equals("")
						|| requestDTO.getEmail() == null || requestDTO.getEmail().equals("")
						|| requestDTO.getPhoneNr() == null || requestDTO.getPhoneNr().equals("")
						|| requestDTO.getMessage() == null || requestDTO.getMessage().equals("")
		) {
			return new ResponseEntity<>("missing_input", HttpStatus.NO_CONTENT);
		}

		// Check if the e-mail has a valid pattern (passed all my tests so far)
		if (!ValidationUtil.isValidEmail(requestDTO.getEmail())) {
			return new ResponseEntity<>("invalid_mail_pattern", HttpStatus.NOT_ACCEPTABLE);
		}

		// Check if the e-mail has a valid pattern (passed all my tests so far)
		if (!ValidationUtil.isValidPhoneNr(requestDTO.getPhoneNr())) {
			return new ResponseEntity<>("invalid_phone_pattern", HttpStatus.NOT_ACCEPTABLE);
		}

		// Check if startDate > endDate
		if (requestDTO.getEventDateStart().after(requestDTO.getEventDateEnd())) {
			return new ResponseEntity<>("start_after_end", HttpStatus.NOT_ACCEPTABLE);
		}

		// Check if selected time span collides with existing events
		if (eventRepository.dateNotAvailable(requestDTO.getEventDateStart(), requestDTO.getEventDateEnd())) {
			return new ResponseEntity<>("not_available", HttpStatus.ACCEPTED);
		}

		requestDTO.setNew(true);
		requestRepository.addRequest(requestDTO.toRequest());

		return new ResponseEntity<>("success", HttpStatus.CREATED);
	}

/*
	PUT
*/

	public ResponseEntity<String> updateRequestData(RequestDTO requestDTO) {
		if (requestRepository.getRequestById(requestDTO.getId()) == null) {
			return new ResponseEntity<>("not found", HttpStatus.NOT_FOUND);
		}

		requestRepository.updateRequestData(requestDTO.toRequest());

		return new ResponseEntity<>("success", HttpStatus.OK);
	}

/*
	DELETE
 */

	public ResponseEntity<String> deleteRequest(Integer id) {
		if (requestRepository.getRequestById(id) == null) {
			return new ResponseEntity<>("not found", HttpStatus.NOT_FOUND);
		}

		requestRepository.deleteRequest(requestRepository.getRequestById(id));

		return new ResponseEntity<>("success", HttpStatus.OK);
	}

/*
	GET
*/

	public List<RequestDTO> getRequests() {
		List<RequestDTO> requestDTOList = new ArrayList<>();
		List<Request> requestList = requestRepository.getRequests();

		if (requestList == null) {
			return null;
		}

		for (Request request : requestList) {
			requestDTOList.add(request.toRequestDTO());
		}

		return requestDTOList;
	}

	public PaginatedContentDTO<RequestDTO> getRequestsPaginated(Integer page) {
		int requestsPerPage = 10;
		List<Request> requestList = requestRepository.getRequests();

		if (requestList == null) {
			return null;
		}

		int totalRequests = requestList.size();
		int totalPages = (int) Math.ceil((float) totalRequests / requestsPerPage);
		int setPage = (page == null || page > totalPages || page < 0) ? 1 : page;

		List<Request> requestListPaginated = requestRepository.getRequestPaginated(setPage);
		List<RequestDTO> requestDTOListPaginated = new ArrayList<>();
		for (Request request : requestListPaginated) {
			requestDTOListPaginated.add(request.toRequestDTO());
		}

		return new PaginatedContentDTO<>(totalPages, requestDTOListPaginated);
	}

	public RequestDTO getRequestById(Integer id) {
		if (requestRepository.getRequestById(id) == null) {
			return null;
		}

		return requestRepository.getRequestById(id).toRequestDTO();
	}

	public List<RequestDTO> getRequestsByName(String name) {
		if (requestRepository.getRequestsByName(name) == null) {
			return null;
		}

		List<RequestDTO> requestDTOList = new ArrayList<>();
		for (int i = 0; i < requestRepository.getRequestsByName(name).size(); i++) {
			requestDTOList.add(requestRepository.getRequestsByName(name).get(i).toRequestDTO());
		}

		return requestDTOList;
	}

}
