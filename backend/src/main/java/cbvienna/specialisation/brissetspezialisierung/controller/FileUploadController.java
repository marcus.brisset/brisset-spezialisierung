package cbvienna.specialisation.brissetspezialisierung.controller;

import cbvienna.specialisation.brissetspezialisierung.dto.EventShortDTO;
import cbvienna.specialisation.brissetspezialisierung.service.EventService;
import cbvienna.specialisation.brissetspezialisierung.util.FileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
@RequestMapping(path = "/api/admin")
public class FileUploadController {

	@Autowired
	EventService eventService;

	@PostMapping(path = "events/upload")
	@ResponseBody
	public ResponseEntity<String> uploadFile(@RequestParam("file") MultipartFile file) {
		try {
			createDirIfNotExist();

			byte[] bytes;
			bytes = file.getBytes();
			Files.write(Paths.get(FileUtil.folderPath + file.getOriginalFilename()), bytes);
			return new ResponseEntity<>(file.getOriginalFilename(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>("failure: " + e.getMessage(), HttpStatus.PAYLOAD_TOO_LARGE);
		}
	}

	@DeleteMapping(path = "flyers/delete/{filename}")
	@ResponseBody
	public ResponseEntity<String> deleteFlyer(@PathVariable String filename) {
		try {
			File file = new File(Paths.get(FileUtil.folderPath) + "/" + filename);
			if (file.delete()) {
				return new ResponseEntity<>("deletion_success", HttpStatus.OK);
			} else {
				return new ResponseEntity<>("deletion_failure", HttpStatus.CONFLICT);
			}
		} catch (Exception e) {
			return new ResponseEntity<>("failure: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


	@GetMapping(path = "flyers/list")
	@ResponseBody
	public Map<String, List<EventShortDTO>> getListFiles() {
		String[] fileList = new java.io.File(FileUtil.folderPath).list();
		Map<String, List<EventShortDTO>> flyerList = new HashMap<>();

		if (fileList == null) {
			return null;
		}

		for (String file : fileList) {
			flyerList.put(file, eventService.eventsUsingFlyer(file));
		}
		return flyerList;
	}

	private void createDirIfNotExist() {
		File directory = new File(FileUtil.folderPath);
		if (!directory.exists()) {
			directory.mkdir();
		}
	}

}
