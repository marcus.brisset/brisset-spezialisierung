package cbvienna.specialisation.brissetspezialisierung.controller;

import cbvienna.specialisation.brissetspezialisierung.dto.AttendedEventsDTO;
import cbvienna.specialisation.brissetspezialisierung.service.AttendedEventsService;
import cbvienna.specialisation.brissetspezialisierung.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class AttendedEventsController {

	@Autowired
	AttendedEventsService attendedEventsService;

/*
	POST
*/

	@CrossOrigin
	@PostMapping(path = "attendance/add")
	@ResponseBody
	public ResponseEntity<String> addAttendedEvent(@RequestBody AttendedEventsDTO attendedEventsDTO) {
		try {
			return attendedEventsService.addAttendedEvent(attendedEventsDTO);
		} catch (Exception e) {
			return new ResponseEntity<>("failure: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@PostMapping(path = "attendance/remove")
	@ResponseBody
	public ResponseEntity<String> removeAttendedEvent(@RequestBody AttendedEventsDTO attendedEventsDTO) {
		try {
			return attendedEventsService.removeAttendedEvent(attendedEventsDTO);
		} catch (Exception e) {
			return new ResponseEntity<>("failure: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


/*
	GET
*/

	@CrossOrigin
	@GetMapping(path = "attendance/{eventId}/{userId}")
	@ResponseBody
	public ResponseEntity<?> checkAttendance(@PathVariable Integer userId, @PathVariable Integer eventId) {
		try {
			return attendedEventsService.checkAttendance(userId, eventId);
		} catch (Exception e) {
			return new ResponseEntity<>("failure: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
