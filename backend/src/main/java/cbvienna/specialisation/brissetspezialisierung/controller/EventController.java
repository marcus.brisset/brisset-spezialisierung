package cbvienna.specialisation.brissetspezialisierung.controller;

import cbvienna.specialisation.brissetspezialisierung.dto.EventDTO;
import cbvienna.specialisation.brissetspezialisierung.dto.EventTypeDTO;
import cbvienna.specialisation.brissetspezialisierung.dto.PaginatedContentDTO;
import cbvienna.specialisation.brissetspezialisierung.dto.PostEventDTO;
import cbvienna.specialisation.brissetspezialisierung.service.EventService;
import cbvienna.specialisation.brissetspezialisierung.service.EventTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/api")
public class EventController {

	@Autowired
	EventService eventService;

	@Autowired
	EventTypeService eventTypeService;

/*
	POST
*/

	// ADMIN
	@CrossOrigin
	@PostMapping(path = "admin/events/post")
	@ResponseBody
	public ResponseEntity<String> insertNewEvent(@RequestBody PostEventDTO postEventDTO) {

		EventTypeDTO eventTypeDTO = eventTypeService.getEventTypeById(postEventDTO.getEventTypeId());

		if (eventTypeDTO == null) {
			return new ResponseEntity<>("not found", HttpStatus.NOT_FOUND);
		}

		EventDTO eventDTO = new EventDTO();

		eventDTO.setEventType(eventTypeDTO.toEventType());
		eventDTO.setTitle(postEventDTO.getTitle());
		eventDTO.setDateStart(postEventDTO.getDateStart());
		eventDTO.setDateEnd(postEventDTO.getDateEnd());
		eventDTO.setPrice(postEventDTO.getPrice());
		eventDTO.setDoorsOpen(postEventDTO.getDoorsOpen());
		eventDTO.setImage(postEventDTO.getImage());
		eventDTO.setText(postEventDTO.getText());

		try {
			return eventService.addEvent(eventDTO);
		} catch (Exception e) {
			return new ResponseEntity<>("failure: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

/*
	PUT
*/

	// ADMIN
	@CrossOrigin
	@PutMapping(path = "admin/events/update")
	@ResponseBody
	public ResponseEntity<String> updateEventData(@RequestBody PostEventDTO postEventDTO) {
		try {
			return eventService.updateEventData(postEventDTO);
		} catch (Exception e) {
			return new ResponseEntity<>("failure: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

/*
	DELETE
*/

	// ADMIN
	@CrossOrigin
	@DeleteMapping(path = "admin/events/delete/{id}")
	@ResponseBody
	public ResponseEntity<String> deleteEvent(@PathVariable Integer id) {
		try {
			return eventService.deleteEvent(id);
		} catch (Exception e) {
			return new ResponseEntity<>("failure: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

/*
	GET
*/

	@CrossOrigin
	@GetMapping(path = "events")
	public List<EventDTO> getEvents() {
		return eventService.getEvents();
	}

	@CrossOrigin
	@GetMapping(path = "events/page/{page}")
	public PaginatedContentDTO<EventDTO> getEventsPaginated(@PathVariable Integer page) {
		return eventService.getEventsPaginated(page);
	}

	@CrossOrigin
	@GetMapping(path = "event/{id}")
	public EventDTO getEventById(@PathVariable Integer id) {
		return eventService.getEventById(id);
	}

	@CrossOrigin
	@GetMapping(path = "event/title/{title}")
	public List<EventDTO> getEventsByTitle(@PathVariable String title) {
		return eventService.getEventsByTitle(title);
	}

	@CrossOrigin
	@GetMapping(path = "events/search/{searchTerm}")
	public List<EventDTO> getEventsBySearchTerm(@PathVariable String searchTerm) {
		return eventService.getEventsBySearchTerm(searchTerm);
	}

	@CrossOrigin
	@GetMapping(path = "events/month/{year}/{month}")
	public List<EventDTO> getEventsByMonth(@PathVariable Integer year, @PathVariable Integer month) {
		return eventService.getEventsByMonth(year, month);
	}

	@CrossOrigin
	@GetMapping(path = "calendar/{year}/{month}")
	public Map<String, Object> getEventCalendar(@PathVariable Integer year, @PathVariable Integer month) {
		return eventService.getEventCalendar(year, month);
	}

}
