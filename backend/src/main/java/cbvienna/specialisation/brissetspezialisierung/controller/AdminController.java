package cbvienna.specialisation.brissetspezialisierung.controller;

import cbvienna.specialisation.brissetspezialisierung.dto.EventDTO;
import cbvienna.specialisation.brissetspezialisierung.dto.StatisticsDTO;
import cbvienna.specialisation.brissetspezialisierung.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/api/admin")
public class AdminController {

	@Autowired
	AdminService adminService;

	@GetMapping(path = "statistics")
	public StatisticsDTO getStatistics() {
		return adminService.getStatistics();
	}

}
