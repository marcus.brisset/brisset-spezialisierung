package cbvienna.specialisation.brissetspezialisierung.controller;

import cbvienna.specialisation.brissetspezialisierung.dto.CommentDTO;
import cbvienna.specialisation.brissetspezialisierung.dto.PaginatedContentDTO;
import cbvienna.specialisation.brissetspezialisierung.dto.PostCommentDTO;
import cbvienna.specialisation.brissetspezialisierung.service.CommentService;
import cbvienna.specialisation.brissetspezialisierung.service.EventService;
import cbvienna.specialisation.brissetspezialisierung.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class CommentController {

	@Autowired
	CommentService commentService;
	@Autowired
	EventService eventService;
	@Autowired
	UserService userService;


/*
	POST
*/

	@CrossOrigin
	@PostMapping(path = "comment")
	@ResponseBody
	public ResponseEntity<String> insertNewComment(@RequestBody PostCommentDTO postCommentDTO) {
		CommentDTO commentDTO = new CommentDTO();

		try {
			commentDTO.setEvent(eventService.getEventById(postCommentDTO.getEventId()).toEvent());
			commentDTO.setUser(userService.getUserById(postCommentDTO.getUserId()).toUser());
		} catch (NullPointerException e){
			return new ResponseEntity<>("invalid event/user id", HttpStatus.NOT_FOUND);
		}

		try {
			commentDTO.setText(postCommentDTO.getComment());
			return commentService.addComment(commentDTO);
		} catch (Exception e) {
			return new ResponseEntity<>("failure: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

/*
	PUT
*/

	@CrossOrigin
	@PutMapping(path = "comment/update")
	@ResponseBody
	public ResponseEntity<String> updateCommentData(Integer commentId, String text) {
		CommentDTO commentDTO = commentService.getCommentById(commentId);

		if (commentDTO == null) {
			return new ResponseEntity<>("not found:", HttpStatus.NOT_FOUND);
		}

		commentDTO.setText(text);
		try {
			return commentService.updateCommentData(commentDTO);
		} catch (Exception e) {
			return new ResponseEntity<>("failure: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

/*
	DELETE
 */

	@CrossOrigin
	@DeleteMapping(path = "comment/delete/{id}")
	@ResponseBody
	public ResponseEntity<String> deleteComment(@PathVariable Integer id) {
		try {
			return commentService.deleteComment(id);
		} catch (Exception e) {
			return new ResponseEntity<>("failure: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}


/*
	GET
*/

	@CrossOrigin
	@GetMapping(path = "comments")
	public List<CommentDTO> getComments() {
		return commentService.getComments();
	}

	@CrossOrigin
	@GetMapping(path = "comment/{id}")
	public CommentDTO getCommentById(@PathVariable Integer id) {
		return commentService.getCommentById(id);
	}

	@CrossOrigin
	@GetMapping(path = "comments/event/{id}")
	public List<CommentDTO> getCommentsOnEvent(@PathVariable Integer id) {
		return commentService.getCommentsOnEvent(id);
	}

	@CrossOrigin
	@GetMapping(path = "comments/event/{id}/{page}")
	public PaginatedContentDTO<CommentDTO> getCommentsOnEventPaged(@PathVariable Integer id, @PathVariable Integer page) {
		return commentService.getCommentsOnEventPaginated(id, page);
	}

}
