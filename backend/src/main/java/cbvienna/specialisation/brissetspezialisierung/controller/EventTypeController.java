package cbvienna.specialisation.brissetspezialisierung.controller;

import cbvienna.specialisation.brissetspezialisierung.dto.EventTypeDTO;
import cbvienna.specialisation.brissetspezialisierung.service.EventTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class EventTypeController {

	@Autowired
	EventTypeService eventTypeService;

/*
	POST
*/

	// ADMIN
	@CrossOrigin
	@PostMapping(path = "admin/eventTypes/post")
	@ResponseBody
	public ResponseEntity<String> insertNewEventType(@RequestBody EventTypeDTO eventTypeDTO) {
		try {
			return eventTypeService.addEventType(eventTypeDTO);
		} catch (Exception e) {
			return new ResponseEntity<>("failure: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

/*
	PUT
*/

	// ADMIN
	@CrossOrigin
	@PutMapping(path = "admin/eventTypes/update")
	@ResponseBody
	public ResponseEntity<String> updateEventTypeData(@RequestBody EventTypeDTO eventTypeDTO) {
		try {
			return eventTypeService.updateEventTypeData(eventTypeDTO);
		} catch (Exception e) {
			return new ResponseEntity<>("failure: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

/*
	DELETE
 */

	// ADMIN
	@CrossOrigin
	@DeleteMapping(path = "admin/eventTypes/delete/{id}")
	@ResponseBody
	public ResponseEntity<String> deleteEventType(@PathVariable Integer id) {
		try {
			return eventTypeService.deleteEventType(id);
		} catch (Exception e) {
			return new ResponseEntity<>("failure: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

/*
	GET
*/

	@CrossOrigin
	@GetMapping(path = "eventTypes")
	public List<EventTypeDTO> getAllEventTypes() {
		return eventTypeService.getEventTypes();
	}

	@CrossOrigin
	@GetMapping(path = "eventType/{id}")
	public EventTypeDTO getEventTypeById(@PathVariable Integer id) {
		return eventTypeService.getEventTypeById(id);
	}

	@CrossOrigin
	@GetMapping(path = "eventType/title/{title}")
	public List<EventTypeDTO> getEventTypesByTitle(@PathVariable String title) {
		return eventTypeService.getEventTypesByTitle(title);
	}

}
