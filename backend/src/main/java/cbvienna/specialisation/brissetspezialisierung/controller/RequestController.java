package cbvienna.specialisation.brissetspezialisierung.controller;

import cbvienna.specialisation.brissetspezialisierung.dto.PaginatedContentDTO;
import cbvienna.specialisation.brissetspezialisierung.dto.RequestDTO;
import cbvienna.specialisation.brissetspezialisierung.repository.RequestRepository;
import cbvienna.specialisation.brissetspezialisierung.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RequestController {

	@Autowired
	RequestService requestService;

	@Autowired
	RequestRepository requestRepository;

/*
	POST
*/

	@CrossOrigin
	@PostMapping(path = "request")
	@ResponseBody
	public ResponseEntity<String> insertNewRequest(@RequestBody RequestDTO requestDTO) {
		try {
			return requestService.addRequest(requestDTO);
		} catch (Exception e) {
			return  new ResponseEntity<>("failure: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

/*
	PUT
*/

	// ADMIN (TO SET BOOLEAN)
	@CrossOrigin
	@PutMapping(path = "admin/requests/update")
	@ResponseBody
	public ResponseEntity<String> updateRequestData(RequestDTO requestDTO) {
		try {
			return requestService.updateRequestData(requestDTO);
		} catch (Exception e) {
			return new ResponseEntity<>("failure: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

/*
	DELETE
*/

	// ADMIN
	@CrossOrigin
	@DeleteMapping(path = "admin/requests/delete/{id}")
	@ResponseBody
	public ResponseEntity<String> deleteRequest(@PathVariable Integer id) {
		try {
			return requestService.deleteRequest(id);
		} catch (Exception e) {
			return new ResponseEntity<>("failure: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

/*
	GET
*/

	// ADMIN
	@CrossOrigin
	@GetMapping(path = "admin/requests")
	public List<RequestDTO> getAllRequests() {
		return requestService.getRequests();
	}

	@CrossOrigin
	@GetMapping(path = "admin/requests/list/{page}")
	public PaginatedContentDTO<RequestDTO> getRequestsPaginated(@PathVariable Integer page) {
		return requestService.getRequestsPaginated(page);
	}

	@CrossOrigin
	@GetMapping(path = "admin/requests/{id}")
	public RequestDTO getRequestById(@PathVariable Integer id) {
		return requestService.getRequestById(id);
	}

	@CrossOrigin
	@GetMapping(path = "admin/requests/byName/{name}")
	public List<RequestDTO> getRequestsByName(@PathVariable String name) {
		return requestService.getRequestsByName(name);
	}
}
