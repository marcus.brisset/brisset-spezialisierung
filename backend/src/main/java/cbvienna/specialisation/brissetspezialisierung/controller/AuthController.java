package cbvienna.specialisation.brissetspezialisierung.controller;

import cbvienna.specialisation.brissetspezialisierung.dto.AuthRequestDTO;
import cbvienna.specialisation.brissetspezialisierung.dto.AuthResponseDTO;
import cbvienna.specialisation.brissetspezialisierung.util.JwtUtil;
import cbvienna.specialisation.brissetspezialisierung.securityconfiguration.MyAuthenticationManager;
import cbvienna.specialisation.brissetspezialisierung.securityconfiguration.MyUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class AuthController {

	@Autowired
	private MyUserDetailsService myUserDetailsService;

	@Autowired
	private MyAuthenticationManager myAuthenticationManager;

	@Autowired
	private JwtUtil jwtUtil;

	@CrossOrigin
	@PostMapping(path = "login")
	@ResponseBody
	public ResponseEntity<?> performLogin(@RequestBody AuthRequestDTO authRequestDTO)
			throws UsernameNotFoundException, NullPointerException, BadCredentialsException {
		try {
			UsernamePasswordAuthenticationToken authenticationToken =
					new UsernamePasswordAuthenticationToken(authRequestDTO.getEmail(), authRequestDTO.getPassword());
			// throws exceptions if user not found or password is incorrect
			myAuthenticationManager.authenticate(authenticationToken);

			final UserDetails userDetails = myUserDetailsService.loadUserByUsername(authRequestDTO.getEmail());
			final String jwt = jwtUtil.generateToken(userDetails);

			return new ResponseEntity<>(new AuthResponseDTO(jwt), HttpStatus.OK);
		} catch (UsernameNotFoundException | NullPointerException e) {
			return new ResponseEntity<>("not found", HttpStatus.NOT_FOUND);
		} catch (BadCredentialsException e) {
			return new ResponseEntity<>("wrong password", HttpStatus.FORBIDDEN);
		}
	}

}
