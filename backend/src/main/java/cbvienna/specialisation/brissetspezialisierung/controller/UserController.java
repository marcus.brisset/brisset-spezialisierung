package cbvienna.specialisation.brissetspezialisierung.controller;

import cbvienna.specialisation.brissetspezialisierung.dto.PaginatedContentDTO;
import cbvienna.specialisation.brissetspezialisierung.dto.UserDTO;
import cbvienna.specialisation.brissetspezialisierung.dto.UserRegistrationDTO;
import cbvienna.specialisation.brissetspezialisierung.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class UserController {

	@Autowired
	UserService userService;

/*
	POST
*/

	@CrossOrigin
	@PostMapping(path = "register")
	@ResponseBody
	public ResponseEntity<String> addUser(@RequestBody UserRegistrationDTO userRegistrationDTO) {
		try {
			return userService.addUser(userRegistrationDTO);
		} catch (DataIntegrityViolationException e) {
			return new ResponseEntity<>("duplicate", HttpStatus.CONFLICT);
		} catch (Exception e) {
			return new ResponseEntity<>("failure: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

/*
	PUT
*/

	@CrossOrigin
	@PutMapping(path = "user/update")
	@ResponseBody
	public ResponseEntity<String> updateUserData(@RequestBody UserDTO updateUserDTO) {
		UserDTO userDTO = userService.getUserById(updateUserDTO.getId());

		if (userDTO == null) {
			return new ResponseEntity<>("not found", HttpStatus.NOT_FOUND);
		}

		if (updateUserDTO.getName() == null || updateUserDTO.getName().trim().equals("")) {
			return new ResponseEntity<>("user name empty", HttpStatus.BAD_REQUEST);
		}

		userDTO.setName(updateUserDTO.getName());

		try {
			return userService.updateUserData(userDTO);
		} catch (Exception e) {
			return new ResponseEntity<>("failure: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

/*
	DELETE
 */

	//	ADMIN
	@CrossOrigin
	@DeleteMapping(path = "admin/users/delete/{id}")
	@ResponseBody
	public ResponseEntity<String> deleteUser(@PathVariable Integer id) {
		try {
			return userService.deleteUser(id);
		} catch (Exception e) {
			return new ResponseEntity<>("failure: " + e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

/*
	GET
*/

	//	ADMIN
	@CrossOrigin
	@GetMapping(path = "admin/users/list")
	public List<UserDTO> getUsers() {
		return userService.getUsers();
	}

	@CrossOrigin
	@GetMapping(path = "admin/users/list/{page}")
	public PaginatedContentDTO<UserDTO> getUsersPaginated(@PathVariable Integer page) {
		return userService.getUsersPaginated(page);
	}

	@CrossOrigin
	@GetMapping(path = "user/id/{id}")
	public UserDTO getUserById(@PathVariable Integer id) {
		return userService.getUserById(id);
	}

	@CrossOrigin
	@GetMapping(path = "user/name/{name}")
	public List<UserDTO> getUsersByName(@PathVariable String name) {
		return userService.getUsersByName(name);
	}

}
