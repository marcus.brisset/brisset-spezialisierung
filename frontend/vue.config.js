/*
*   AWS credentials pulled from ~/.aws/credentials
*   Needed: Access Key _AND_ Secret Key
*   If the secret key gets lost, there is no way to retrieve it again
*   > generate new access key pair and set the old one inactive
*/

module.exports = {
  pluginOptions: {
    s3Deploy: {
      registry: undefined,
      awsProfile: 'default',
      overrideEndpoint: false,
      //endpoint: 'Cbprojectfrontend-env.eba-hsf9u6np.eu-central-1.elasticbeanstalk.com',
      region: 'eu-central-1',
      //bucket: 'elasticbeanstalk-eu-central-1-925259229751',
      bucket: 'codersbay.brisset.at',
      createBucket: false,
      staticHosting: true,
      staticIndexPage: 'index.html',
      staticErrorPage: 'index.html',
      assetPath: 'dist',
      assetMatch: '**',
      deployPath: '/',
      acl: 'public-read',
      pwa: false,
      enableCloudfront: false,
      //cloudfrontId: "E31FETGBDW6ML3",
      pluginVersion: '4.0.0-rc3',
      uploadConcurrency: 5
    }
  }
}
