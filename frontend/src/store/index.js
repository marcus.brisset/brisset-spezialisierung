import Vue from "vue";
import Vuex from "vuex";
import jwtDecode from "jwt-decode";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		user: {
			userEmail: null,
			userId: null,
			userName: null,
			auth: null
		},
		allEventTypes: {},
		allUsers: {},
		allEvents: {},
		allRequests: {},
		statistics: {}
	},
	getters: {
		isLoggedIn: state => state.user.userName !== null,
		isAdmin: state => state.user.auth === "ADMIN",

		getUserEmail: state => state.user.userEmail,
		getUserName: state => state.user.userName,
		getUserId: state => state.user.userId,
		getUserAuth: state => state.user.auth,

		getAllEventTypes: state => state.allEventTypes,
		getAllUsers: state => state.allUsers,
		getAllEvents: state => state.allEvents,
		getAllRequests: state => state.allRequests,
		getStatistics: state => state.statistics
	},
	mutations: {
		setAuthorization(state, authorization) {
			let decodedJwt = jwtDecode(authorization.jwt);
			state.user.userEmail = decodedJwt["sub"];
			state.user.userId = decodedJwt["userId"];
			state.user.userName = decodedJwt["userName"];
			state.user.auth = decodedJwt["roles"][0].authority;
			localStorage.setItem("jwt", authorization.jwt);
		},
		unsetAuthorization(state) {
			Object.keys(state.user).forEach( key => {
				state.user[key] = null;
			});
			localStorage.removeItem("jwt");
		},

		setAllEventTypes(state, allEventTypes) {
			state.allEventTypes = allEventTypes;
		},

		setAllUsers(state, allUsers) {
			state.allUsers = allUsers;
		},

		setAllEvents(state, allEvents) {
			state.allEvents = allEvents;
		},

		setAllRequests(state, allRequests) {
			state.allRequests = allRequests;
		},

		setStatistics(state, statistics) {
			state.statistics = statistics;
		}
	},
	actions: {
		login({ commit }, jwt) {
			commit("setAuthorization", jwt);
		},

		logout({ commit }) {
			commit("unsetAuthorization");
		},

		async getAllEventTypes({ commit }) {
			const response = await axios.get("/api/eventTypes");
			commit("setAllEventTypes", response.data);
		},

		async getAllUsers({ commit }) {
			const response = await axios.get("/api/admin/users/list");
			commit("setAllUsers", response.data);
		},

		async getAllEvents({ commit }) {
			const response = await axios.get("/api/events");
			commit("setAllEvents", response.data);
		},

		async getAllRequests({ commit }) {
			const response = await axios.get("/api/admin/requests")
			commit("setAllRequests", response.data);
		},

		async getStatistics({ commit }) {
			const response = await axios.get("/api/admin/statistics");
			commit("setStatistics", response.data);
		}
	}
});