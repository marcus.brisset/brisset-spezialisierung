import Vue from 'vue';
import VueRouter from "vue-router";
import store from "@/store";

import Home from "@/views/Home";
import Program from "@/views/Program";
import Location from "@/views/Location";
import Bookings from "@/views/Bookings";
import Faq from "@/views/Faq";
import Register from "@/views/Register";
import EventSingle from "@/views/EventSingle";
import SearchEvents from "@/views/SearchEvents";

import UserSettings from "@/views/UserSettings";

import Admin from "@/views/Admin";
import AdminEventTypes from "@/views/admin/AdminEventTypes";
import AdminEvents from "@/views/admin/AdminEvents";
import AdminEventsEdit from "@/views/admin/AdminEventsEdit";
import AdminEventsNew from "@/views/admin/AdminEventsNew";
import AdminFlyers from "@/views/admin/AdminFlyers";
import AdminUsers from "@/views/admin/AdminUsers";
import AdminRequests from "@/views/admin/AdminRequests";
import AdminRequestsSingle from "@/views/admin/AdminRequestsSingle";
import AdminStatistics from "@/views/admin/AdminStatistics";

import jwtDecode from "jwt-decode";

Vue.use(VueRouter);

const routes = [
    //lazy loading () => import("../components/Program.vue")
    { path: "/", component: Home },
    { path: "/home", component: Home },
    { path: "/location", component: Location },
    { path: "/program", component: Program },
    { path: "/program/:year/:month", component: Program, props: true },
    { path: "/event/:id", component: EventSingle, props: true },
    { path: "/event/:id/:page", component: EventSingle, props: true },
    { path: "/search/:term", component: SearchEvents, props: true },
    { path: "/bookings", component: Bookings },
    { path: "/bookings/:setDate", component: Bookings, props: true },
    { path: "/faq", component: Faq },
    { path: "/register", component: Register },
    {
        path: "/user/settings",
        component: UserSettings,
        beforeEnter: (to, from, next) => {
            if (!store.getters.isLoggedIn) {
                if (localStorage.getItem("jwt")) {
                    next();
                } else {
                    next("/register");
                }
            } else {
                next();
            }
        }
    },
    {
        path: "/admin",
        component : Admin,
        beforeEnter: (to, from, next) => {
            if (!store.getters.isAdmin) {
                // Fix for refreshing admin pages and not getting booted
                if (localStorage.getItem("jwt")) {
                    let jwtDecoded = jwtDecode(localStorage.getItem("jwt"));
                    if (jwtDecoded["roles"][0].authority === "ADMIN") {
                        next();
                    } else {
                        next("/register");
                    }
                }
            } else {
                next();
            }
        },
        children: [
            {
                path: "eventTypes",
                component: AdminEventTypes
            },
            {
                path: "events",
                component: AdminEvents
            },
            {
                path: "events/new",
                component: AdminEventsNew
            },
            {
                path: "flyers",
                component: AdminFlyers
            },
            {
                path: "events/new/:setDate",
                component: AdminEventsNew,
                props: true
            },
            {
                path: "events/edit/:id",
                component: AdminEventsEdit,
                props: true
            },
            {
                path: "users",
                component: AdminUsers
            },
            {
                path: "requests",
                component: AdminRequests
            },
            {
                path: "requests/detail/:id",
                component: AdminRequestsSingle,
                props: true
            },
            {
                path: "statistics",
                component: AdminStatistics
            }
        ]
    }
];

const router = new VueRouter({
    scrollBehavior(to, from ,savedPosition) {
        if (savedPosition) {
            return savedPosition;
        } else {
            return {
                x: 0,
                y: 0,
                behavior: "smooth"
            };
        }
    },
    routes,
    mode: "history",
});

export default router;
