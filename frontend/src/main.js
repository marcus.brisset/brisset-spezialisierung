import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import App from "./App.vue";
import store from "./store";
import router from "./router";
import pageTitleMixin from "@/mixins/pageTitleMixin";
import jwtDecode from "jwt-decode";

Vue.config.productionTip = false;

Vue.use(VueAxios, axios);

axios.defaults.baseURL = process.env.VUE_APP_BASE_URL;

axios.interceptors.request.use(request => {
    if (!request.headers) {
        request.headers = {};
    }
    if (localStorage.getItem("jwt")) {
        // checks if the stored JWT is expired, if so it won't be sent and instead will be deleted
        let now = Math.round((new Date().getTime() / 1000));
        let jwtIsExpired = jwtDecode(localStorage.getItem("jwt")).exp - now < 0;
        if (!jwtIsExpired) {
            request.headers.Authorization = "Bearer " + localStorage.getItem("jwt");
        } else {
            store.dispatch("logout");
        }
    }
    return request;
})

Vue.mixin(pageTitleMixin);
Vue.mixin({

    methods: {
/*
        [IMPORTANT:] Target form MUST be an object named 'form', consisting of the elements input fields,
        and they MUST be referenced by that exact same name. E.g. <input ref="email" v-model="form.email" ...
        TODO: Implement email validation if el === 'email'. Why? Form WILL be submitted even if its input fields fail
         their respective contextual requirements (Because we prevent default behaviours. Looking at you JavaScript -.-)
*/
        globalValidateForm: function() {
            let verified = true;
            let firstOccasion = true;
            for (let el in this.form) {
                if (
                    (typeof this.form[el] === "string" && this.form[el].trim().length === 0)
                    || this.form[el] === ""
                ) {
                    verified = false;
                    this.$refs[el].classList.add("input--alert");
                    if (firstOccasion) {
                        firstOccasion = false;
                        this.$refs[el].focus();
                    }
                } else {
                    if (this.$refs[el]) {
                        this.$refs[el].classList.remove("input--alert");
                    }
                }
            }
            return verified;
        },

        globalResetFormValidation: function() {
            for (let el in this.form) {
                this.$refs[el].classList.remove("input--alert");
            }
        },

        globalEmptyForm: function () {
            for (let el in this.form) {
                this.form[el] = "";
            }
        },

        globalToggleDeleteButtons(value) {
            for (let button in this.$refs.delete) {
                this.$refs.delete[button].disabled = !value;
            }
        },

        globalDateToString(inputDate, time = false) {
            let date = new Date(inputDate)
            let dateString = date.toLocaleDateString("de-DE", {
                weekday: "long",
                day: "numeric",
                month: "long",
                year: "numeric"
            });
            if (time) {
                dateString += " | " + date.toLocaleTimeString("de-DE");
            }
            return dateString;
        }
    }
})

new Vue({
    render: h => h(App),
    store,
    router
}).$mount('#app');
