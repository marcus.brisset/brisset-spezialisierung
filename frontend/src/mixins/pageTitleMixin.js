function getPageTitle(vm) {
    const {title} = vm.$options;
    if (title) {
        return typeof title === "function"
            ? title.call(vm)
            : title;
    }
}

export default {
    created () {
        const pageTitle = getPageTitle(this);
        if (pageTitle) {
            document.title = "FABRIK: " + pageTitle;
        }
    }
}
