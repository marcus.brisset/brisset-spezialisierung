# CODERS.BAY Vienna Full-Stack project.

## [DEMO hosted on AWS](http://codersbay.brisset.at)

Backend needs to run on port 8080, since all ajax requests statically point to localhost:8080.  
SQL database will be generated automatically on first run (database name set to "fabrik").  
Edit db credentials in application.properties (set to username=admin, password=admin).  
When being run for the first time, first user registered will automatically be set to ADMIN.

### Technology stack 
Spring Boot (w/ Spring Security & SQL) && VueJS  

### HTML Prototype
https://mbrisset.bplaced.net/brisset-spezialisierung/public_html/  

All assets created by myself (except the party crowd > [all-silhouettes](https://all-silhouettes.com), possibly down)  
(Software used: Inkscape && Macromedia Fireworks MX 2004)

### In terms of copyrighted content:  
#### F.A.Q.:  
[Forum Wien – ARENA](https://arena.wien)
#### Event Texts and Images:
[MAK](https://mak.at)  
[template.net](https://https://www.template.net)
#### Location Images:
[eventonline.be](https://www.eventonline.be) ([hangar43.be](https://www.hangar43.be))
